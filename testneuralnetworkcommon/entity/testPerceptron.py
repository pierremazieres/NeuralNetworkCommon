#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworkcommon.entity.layer.layer import Layer
from neuralnetworkcommon.database.perceptron import selectById
from neuralnetworkcommon.database.layer import insert, update, selectByPerceptronIdAndDepthIndex
from testneuralnetworkcommon.commonUtilities import CommonTest
from testneuralnetworkcommon.entity.perceptronData import perceptronPassForwardData,perceptronPassBackwardData,perceptronPassForwardBackwardData
# utils
def initialize(layers):
    for layer in layers:
        emptyLayer = Layer(CommonTest.testPerceptronId, layer.depthIndex, None, None)
        insert(CommonTest.cursor,emptyLayer, 0, 0)
        update(CommonTest.cursor,layer)
    CommonTest.connection.commit()
    pass
# test perceptron
class testPerceptron(CommonTest):
    # test perceptron computing
    def testPerceptronPassForward(self):
        # initialize
        layers,input,expectedOutput = perceptronPassForwardData()
        initialize(layers)
        # execute pass forward
        perceptron = selectById(CommonTest.cursor,CommonTest.testPerceptronId,True)
        actualOutput = perceptron.layerTrainingDB.passForward(input)
        perceptron.layerTrainingDB.cleanup()
        actualOutput = [float(_) for _ in actualOutput]
        # check result
        self.assertListAlmostEqual(expectedOutput, actualOutput, "perceptron pass forward output")
        pass
    def testPerceptronPassBackward(self):
        # initialize
        layers,expectedOutput,expectedLayers,trainingDraft = perceptronPassBackwardData()
        initialize(layers)
        # set training draft
        perceptron = selectById(CommonTest.cursor,CommonTest.testPerceptronId,True)
        perceptron.layerTrainingDB.trainingDraft = trainingDraft
        # pass backward
        # INFO : commit needed to see data in cursored loop
        CommonTest.connection.commit()
        perceptron.layerTrainingDB.passBackward(expectedOutput)
        perceptron.layerTrainingDB.cleanup()
        # check result
        for depthIndex in range(0,3):
            layer = selectByPerceptronIdAndDepthIndex(CommonTest.cursor,CommonTest.testPerceptronId,depthIndex)
            self.assertEqual(layer, expectedLayers[depthIndex], "ERROR : pass backward does not match")
        pass
    def testPerceptronPassForwardBackward(self):
        # initialize
        layers,input,expectedOutput,expectedTotalError = perceptronPassForwardBackwardData()
        initialize(layers)
        perceptron = selectById(CommonTest.cursor,CommonTest.testPerceptronId,True)
        actualTotalError = perceptron.passForwardBackward(input,expectedOutput)
        perceptron.layerTrainingDB.cleanup()
        self.assertAlmostEqual(actualTotalError, expectedTotalError, msg="ERROR : pass forward/backward does not match : delta="+str(abs(expectedTotalError-actualTotalError)), delta=1e-15)
        pass
    pass
pass
