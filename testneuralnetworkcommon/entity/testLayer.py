#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworkcommon.utilities.sigmoid import value, derivative
from pythoncommontools.unittest.testCase import TestCase
from testneuralnetworkcommon.entity.layerData import sigmoidValueData,sigmoidDerivativeData,layerDifferentialErrorOutputData,layerComputeNewWeightsData,layerComputeNewBiasesData,layerPassBackwardOutputData,layerPassBackwardHidden,layerPassForward
# test perceptron
class testLayer(TestCase):
    # test sigmoid computing
    def testSigmoidValue(self):
        variables, expectedValue = sigmoidValueData()
        actualValue = value(variables)
        actualValue = [float(_) for _ in actualValue]
        self.assertListEqual(expectedValue, actualValue, "ERROR : sigmoïd value does not match")
        pass
    def testSigmoidDerivative(self):
        variables, expectedValue = sigmoidDerivativeData()
        actualValue = derivative(variables)
        actualValue = [float(_) for _ in actualValue]
        self.assertListEqual(expectedValue, actualValue, "ERROR : sigmoïd value does not match")
        pass
    # test layer computing
    def testLayerDifferentialErrorOutput(self):
        layer,expectedOutput,expectedDifferentialError=layerDifferentialErrorOutputData()
        actualDifferentialError = layer.differentialErrorOutput(expectedOutput)
        actualDifferentialError = [float(_) for _ in actualDifferentialError]
        self.assertListEqual(expectedDifferentialError, actualDifferentialError, "ERROR : differential error output does not match")
        pass
    def testLayerComputeNewWeights(self):
        layer,differentialErrorLayer, expectedNewDifferentialErrorWeightsBiases, expectedOldWeights, expectedNewWeights=layerComputeNewWeightsData()
        actualNewDifferentialErrorWeightsBiases, actualOldWeights = layer.computeNewWeights(differentialErrorLayer)
        actualNewDifferentialErrorWeightsBiases = [[float(__) for __ in _] for _ in actualNewDifferentialErrorWeightsBiases]
        actualOldWeights = [[float(__) for __ in _] for _ in actualOldWeights]
        layer.weights = [[float(__) for __ in _] for _ in layer.weights]
        self.assertListEqual(expectedNewDifferentialErrorWeightsBiases, actualNewDifferentialErrorWeightsBiases, "ERROR : NewDifferentialErrorWeightsBiases does not match")
        self.assertListEqual(expectedOldWeights, actualOldWeights, "ERROR : OldWeights does not match")
        self.assertListEqual(expectedNewWeights, layer.weights, "ERROR : NewWeights does not match")
        pass
    def testLayerComputeNewBiases(self):
        layer, differentialErrorWeightsBiases, expectedBiases=layerComputeNewBiasesData()
        layer.computeNewBiases(differentialErrorWeightsBiases)
        layer.biases = [float(_) for _ in layer.biases]
        self.assertListEqual(expectedBiases, layer.biases, "ERROR : biases does not match")
        pass
    def testLayerPassBackwardOutput(self):
        layer, expectedOutput, expectedNewDifferentialErrorWeightsBiases, expectedOldWeights=layerPassBackwardOutputData()
        actualNewDifferentialErrorWeightsBiases, actualOldWeights = layer.passBackwardLastLayer(expectedOutput)
        actualNewDifferentialErrorWeightsBiases = [[float(__) for __ in _] for _ in actualNewDifferentialErrorWeightsBiases]
        actualOldWeights = [[float(__) for __ in _] for _ in actualOldWeights]
        self.assertListEqual(expectedNewDifferentialErrorWeightsBiases, actualNewDifferentialErrorWeightsBiases, "ERROR : pass backward output actualNewDifferentialErrorWeightsBiases does not match")
        self.assertListEqual(expectedOldWeights, actualOldWeights, "ERROR : pass backward output actualOldWeights does not match")
    def testLayerPassBackwardHidden(self):
        layer, differentialErrorWeightsBiasInput, previousLayerWeights, expectedNewDifferentialErrorWeightsBiases, expectedOldWeights=layerPassBackwardHidden()
        actualNewDifferentialErrorWeightsBiases, actualOldWeights = layer.passBackwardHiddenLayer(differentialErrorWeightsBiasInput,previousLayerWeights)
        actualNewDifferentialErrorWeightsBiases = [[float(__) for __ in _] for _ in actualNewDifferentialErrorWeightsBiases]
        actualOldWeights = [[float(__) for __ in _] for _ in actualOldWeights]
        self.assertListEqual(expectedNewDifferentialErrorWeightsBiases, actualNewDifferentialErrorWeightsBiases, "ERROR : pass backward output actualNewDifferentialErrorWeightsBiases does not match")
        self.assertListEqual(expectedOldWeights, actualOldWeights, "ERROR : pass backward hidden actualOldWeights does not match")
    def testLayerPassForward(self):
        layer, input, expectedOutput=layerPassForward()
        # format output
        actualOutput = layer.passForward(input,True)
        actualOutput = [float(_) for _ in actualOutput]
        layer.trainingDraft.input = [float(_) for _ in layer.trainingDraft.input]
        layer.trainingDraft.output = [float(_) for _ in layer.trainingDraft.output]
        # we can have some round deviation, so we compare each value with a tolerance
        self.assertListAlmostEqual(expectedOutput, actualOutput, "pass forward output")
        self.assertListEqual(input, layer.trainingDraft.input, "ERROR : pass forward trainingDraft.input does not match")
        self.assertListAlmostEqual(expectedOutput, layer.trainingDraft.output, "pass forward trainingDraft.output")
        # test without training
        del layer.trainingDraft
        layer.passForward(input)
        self.assertFalse(hasattr(layer,"trainingDraft"))
        pass
    pass
pass