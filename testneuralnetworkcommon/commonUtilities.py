# coding=utf-8
# import
from neuralnetworkcommon.database.layer import insert as layerInsert, deleteAllByPerceptronId
from neuralnetworkcommon.database.perceptron import insert as perceptronInsert, selectAllIdsByWorkspaceId as perceptronSelectAllIdsByWorkspaceId, deleteAllByWorkspaceId as perceptronDeleteAllByWorkspaceId
from neuralnetworkcommon.database.workspace import insert as workspaceInsert, deleteById
from neuralnetworkcommon.entity.perceptron.perceptron import Perceptron
from neuralnetworkcommon.entity.layer.layer import Layer
from neuralnetworkcommon.entity.workspace import Workspace
from random import randint, choice, random, shuffle
from string import ascii_letters
from pythoncommontools.unittest.testCase import TestCase
from neuralnetworkcommon.service.connectionPooledResource import ConnectionPooledResource
from neuralnetworkcommon.entity.trainingSet.trainingSet import TrainingSet
from neuralnetworkcommon.database.trainingSet import selectAllIdsByWorkspaceId as trainingSetSelectAllIdsByWorkspaceId, deleteAllByWorkspaceId as trainingSetDeleteAllByWorkspaceId, insert as trainingSetInsert
from neuralnetworkcommon.database.trainingElement import deleteAllByTrainingSetId, insert as trainingElementInsert
from neuralnetworkcommon.entity.trainingElement import TrainingElement
from neuralnetworkcommon.entity.trainingSession.trainingSession import TrainingSession
from neuralnetworkcommon.database.trainingSession import deleteAllByWorkspaceId as trainingSessionDeleteAllByWorkspaceId, insert as trainingSessionInsert
from neuralnetworkcommon.entity.trainingSessionProgress import TrainingSessionProgress
from neuralnetworkcommon.database.trainingSessionProgress import deleteAllByWorkspaceId as trainingSessionProgressDeleteAllByWorkspaceId, insert as trainingSessionProgressInsert
from neuralnetworkcommon.database.trainingSessionElement import deleteByPerceptronId, selectAllByPerceptronIdAndFilter
from neuralnetworkcommon.database.trainer import TrainerDB
# test utilities
# INFO : those utilities are not merged with common neural network one because test modules are not deployed in libraries
def randomWorkspace(id=None):
    comments = ''.join([choice(ascii_letters) for _ in range(15)])
    workspace = Workspace(id, comments)
    return workspace
def insertRandomWorkspaces():
    workspaceIds = set()
    workspaceNumber = randint(5, 10)
    for _ in range(workspaceNumber):
        workspace = randomWorkspace()
        workspaceInsert(CommonTest.cursor,workspace)
        workspaceId = workspace.id
        workspaceIds.add(workspaceId)
    return workspaceIds
def randomPerceptron(id=None,workspaceId=None):
    comments = ''.join([choice(ascii_letters) for _ in range(15)])
    if not workspaceId:
        workspaceId = CommonTest.testWorkspaceId
    perceptron = Perceptron(id,workspaceId,comments)
    return perceptron
def insertRandomPerceptrons():
    perceptronIds = set()
    perceptronNumber = randint(5, 10)
    for _ in range(perceptronNumber):
        perceptron = randomPerceptron()
        perceptronInsert(CommonTest.cursor,perceptron)
        perceptronIds.add(perceptron.id)
    return perceptronIds
def randomLayer(perceptronId,depthIndex,previousDimension=0,currentDimension=0):
    weights = [[(random() - .5) * 2 for _ in range(previousDimension)] for _ in range(currentDimension)]
    biases = [random()] * currentDimension
    layer = Layer(perceptronId,depthIndex,weights,biases)
    return layer
def insertRandomLayers(perceptronId,random=False):
    layerNumber = randint(5, 10)
    if random:
        previousDimension = randint(2, 16)
        currentDimension = randint(2, 16)
    else:
        previousDimension = 0
        currentDimension = 0
    for depthIndex in range(layerNumber):
        layer = randomLayer(perceptronId,depthIndex)
        layerInsert(CommonTest.cursor,layer, previousDimension,currentDimension)
        if random :
            previousDimension = currentDimension
            currentDimension = randint(2, 16)
    return layerNumber
def randomTrainingSet(id=None):
    comments = ''.join([choice(ascii_letters) for _ in range(15)])
    trainingSet = TrainingSet(id, CommonTest.testWorkspaceId, comments=comments)
    return trainingSet
def insertRandomTrainingSets():
    trainingSetsIds = set()
    trainingSetNumber = randint(5, 10)
    for _ in range(trainingSetNumber):
        trainingSet = randomTrainingSet()
        trainingSetInsert(CommonTest.cursor,trainingSet)
        trainingSetsIds.add(trainingSet.id)
        pass
    return trainingSetsIds
def randomTrainingElement(id=None,trainingSetId=None,inputDimension=randint(20, 100), outputDimension=randint(20, 100)):
    # generate training elements
    input = [(random() - .5) * 2 for _ in range(inputDimension)]
    expectedOutput = [(random() - .5) * 2 for _ in range(outputDimension)]
    trainingElement = TrainingElement(id,trainingSetId,input,expectedOutput)
    return trainingElement
def insertRandomTrainingElements(trainingSetId,inputDimension=randint(20, 100), outputDimension=randint(20, 100)):
    trainingElementsIds = set()
    trainingElementNumber = randint(100, 200)
    for _ in range(trainingElementNumber):
        trainingElement = randomTrainingElement(trainingSetId=trainingSetId,inputDimension=inputDimension,outputDimension=outputDimension)
        trainingElementInsert(CommonTest.cursor,trainingElement)
        trainingElementsIds.add(trainingElement.id)
        pass
    return trainingElementsIds
def randomTrainingSession(perceptronId,trainingSetId,pid=randint(1,1e3),errorMessage=''.join([choice(ascii_letters) for _ in range(50)]),testScore=random()):
    comments = ''.join([choice(ascii_letters) for _ in range(15)])
    trainingSession = TrainingSession(perceptronId,trainingSetId,saveInterval=randint(1,10),maximumTry=randint(1,10),maximumErrorRatio=random(),pid=pid,errorMessage=errorMessage,testScore=testScore,comments=comments)
    return trainingSession
def insertRandomTrainingSessions():
    perceptronIds = insertRandomPerceptrons()
    for perceptronId in perceptronIds:
        trainingSession = randomTrainingSession(perceptronId, CommonTest.testTrainingSetId)
        trainingSessionInsert(CommonTest.cursor,trainingSession,0)
    return perceptronIds
def randomTrainingSessionProgress(perceptronId,progressNumber = randint(20, 100)):
    trainingSessionProgress = TrainingSessionProgress(perceptronId,[random()]*progressNumber, [randint(20, 100)]*progressNumber, [True if random()>0.5 else False]*progressNumber)
    return trainingSessionProgress
def insertRandomTrainingSessionProgresses():
    perceptronIds = insertRandomPerceptrons()
    for perceptronId in perceptronIds:
        trainingSessionProgress = randomTrainingSessionProgress(perceptronId)
        trainingSessionProgressInsert(CommonTest.cursor,trainingSessionProgress)
    return perceptronIds
def randomizeTrainingSessionElementsError(perceptronId):
    trainingSessionElementsIds = selectAllByPerceptronIdAndFilter(CommonTest.cursor,perceptronId)
    shuffle(trainingSessionElementsIds)
    trainingSessionElementsIds = trainingSessionElementsIds[:len(trainingSessionElementsIds) // 2]
    trainerDB = TrainerDB(perceptronId,CommonTest.connection,CommonTest.cursor)
    CommonTest.connection.commit()
    trainerDB.updateElementsOnErrors(trainingSessionElementsIds)
    trainerDB.cursorConnection.commit()
    trainerDB.cleanup()
    pass
# test common class
class CommonTest(TestCase):
    testWorkspaceId = None
    testPerceptronId = None
    testTrainingSetId = None
    connection = None
    cursor = None
    @classmethod
    def setUpClass(cls):
        CommonTest.connection = ConnectionPooledResource.connectionPool.getconn()
        CommonTest.cursor = CommonTest.connection.cursor()
        pass
    @classmethod
    def tearDownClass(cls):
        # INFO : important to commit before (free transaction block)
        CommonTest.connection.commit()
        CommonTest.cursor.close()
        ConnectionPooledResource.connectionPool.putconn(CommonTest.connection)
        pass
    def setUp(cls):
        # INFO : it is important to rebuild test object before each test
        testWorkspace = randomWorkspace()
        workspaceInsert(CommonTest.cursor,testWorkspace)
        CommonTest.testWorkspaceId = testWorkspace.id
        testPerceptron = randomPerceptron()
        perceptronInsert(CommonTest.cursor,testPerceptron)
        CommonTest.testPerceptronId = testPerceptron.id
        testTrainingSet = randomTrainingSet()
        trainingSetInsert(CommonTest.cursor,testTrainingSet)
        CommonTest.testTrainingSetId = testTrainingSet.id
        pass
    @classmethod
    def tearDown(cls):
        # INFO : important to commit (free transaction block) before cleaning
        CommonTest.connection.commit()
        trainingSessionProgressDeleteAllByWorkspaceId(CommonTest.cursor, CommonTest.testWorkspaceId)
        deleteByPerceptronId(CommonTest.cursor, CommonTest.testPerceptronId)
        trainingSessionDeleteAllByWorkspaceId(CommonTest.cursor, CommonTest.testWorkspaceId)
        trainingSetIds = trainingSetSelectAllIdsByWorkspaceId(CommonTest.cursor, CommonTest.testWorkspaceId)
        for trainingSetId in trainingSetIds:
            deleteAllByTrainingSetId(CommonTest.cursor, trainingSetId)
        trainingSetDeleteAllByWorkspaceId(CommonTest.cursor, CommonTest.testWorkspaceId)
        perceptronIds = perceptronSelectAllIdsByWorkspaceId(CommonTest.cursor, CommonTest.testWorkspaceId)
        for perceptronId in perceptronIds:
            deleteAllByPerceptronId(CommonTest.cursor, perceptronId)
        perceptronDeleteAllByWorkspaceId(CommonTest.cursor, CommonTest.testWorkspaceId)
        deleteById(CommonTest.cursor, CommonTest.testWorkspaceId)
        CommonTest.connection.commit()
        pass
pass
