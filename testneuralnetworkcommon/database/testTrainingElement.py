#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworkcommon.database.trainingElement import insert,selectById,update,deleteById,selectAllByTrainingSetIdAndFilter,deleteAllByTrainingSetId
from neuralnetworkcommon.entity.trainingElement import TrainingElement
from testneuralnetworkcommon.commonUtilities import CommonTest, randomTrainingElement, insertRandomTrainingElements
from testpythoncommontools.database.database import checkPostgreSqlDatabaseError
# test training element
badTrainingElement = TrainingElement('', -1, [], [])
class testTrainingElementDB(CommonTest):
    # test CRUD OK
    def testCrudOK(self):
        # initialize random training element
        initialTrainingElement = randomTrainingElement(trainingSetId=CommonTest.testTrainingSetId)
        # precheck
        self.assertIsNone(initialTrainingElement.id,"ERROR : trainingElement has id")
        # call DB insert
        insert(CommonTest.cursor,initialTrainingElement)
        # check DB insert
        trainingElementId = initialTrainingElement.id
        self.assertIsNotNone(trainingElementId,"ERROR : trainingElement has no id")
        # call DB select by id
        fetchedInsertedTrainingElement = selectById(CommonTest.cursor,trainingElementId)
        # check DB select by id
        self.assertEqual(initialTrainingElement,fetchedInsertedTrainingElement,"ERROR : inserted trainingElement does not match")
        # call DB update
        newTrainingElement = randomTrainingElement(trainingElementId,CommonTest.testTrainingSetId)
        update(CommonTest.cursor,newTrainingElement)
        # check DB update
        fetchedUpdatedTrainingElement = selectById(CommonTest.cursor,trainingElementId)
        self.assertNotEqual(fetchedUpdatedTrainingElement,fetchedInsertedTrainingElement,"ERROR : trainingElement not updated")
        self.assertEqual(fetchedUpdatedTrainingElement,newTrainingElement,"ERROR : trainingElement not updated")
        # call DB delete
        deleteById(CommonTest.cursor,trainingElementId)
        # check DB delete
        deletedTrainingElement = selectById(CommonTest.cursor,trainingElementId)
        self.assertIsNone(deletedTrainingElement,"ERROR : trainingSet not deleted")
        pass
    # test select/delete all OK
    def testSelectDeleteAll(self):
        # initialize random training elements
        initialIds = insertRandomTrainingElements(CommonTest.testTrainingSetId)
        # select IDs
        fetchedIds = selectAllByTrainingSetIdAndFilter(CommonTest.cursor,CommonTest.testTrainingSetId)
        expectedFirstId = fetchedIds[0]
        firstElement = selectById(CommonTest.cursor,expectedFirstId)
        fetchedFirstId = set(selectAllByTrainingSetIdAndFilter(CommonTest.cursor,CommonTest.testTrainingSetId,expectedOutput=firstElement.expectedOutput));
        # check IDs
        self.assertTrue(initialIds.issubset(fetchedIds),"ERROR : IDs selection does not match")
        self.assertSetEqual(fetchedFirstId, {expectedFirstId},msg="ERROR : first ID does not match")
        # delete all
        deleteAllByTrainingSetId(CommonTest.cursor,CommonTest.testTrainingSetId)
        # check IDs
        deletedIds = selectAllByTrainingSetIdAndFilter(CommonTest.cursor,CommonTest.testTrainingSetId)
        self.assertEqual(len(deletedIds),0,"ERROR : complete deletion failed")
        pass
    # test training element error
    def testTrainingElementInsertError(self):
        checkPostgreSqlDatabaseError(self, insert, (CommonTest.cursor,badTrainingElement,))
    def testTrainingElementUpdateError(self):
        checkPostgreSqlDatabaseError(self, update, (CommonTest.cursor,badTrainingElement,))
pass
