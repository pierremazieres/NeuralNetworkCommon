#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworkcommon.database.workspace import insert, selectById, update, deleteById, selectAllIds
from testneuralnetworkcommon.commonUtilities import CommonTest, randomWorkspace, insertRandomWorkspaces
# test workspace
class testWorkspaceDB(CommonTest):
    # test CRUD OK
    def testCrudOK(self):
        # initialize random workspace
        initialWorkspace = randomWorkspace()
        # precheck
        self.assertIsNone(initialWorkspace.id,"ERROR : workspace has id")
        # call DB insert
        insert(CommonTest.cursor,initialWorkspace)
        # check DB insert
        self.assertIsNotNone(initialWorkspace.id,"ERROR : workspace has no id")
        # call DB select by id
        fetchedInsertedWorkspace = selectById(CommonTest.cursor,initialWorkspace.id)
        # check DB select by id
        self.assertEqual(initialWorkspace,fetchedInsertedWorkspace,"ERROR : inserted workspace does not match")
        # call DB update
        newWorkspace = randomWorkspace()
        newWorkspace.id = initialWorkspace.id
        update(CommonTest.cursor,newWorkspace)
        # check DB update
        fetchedUpdatedWorkspace = selectById(CommonTest.cursor,initialWorkspace.id)
        self.assertNotEqual(fetchedUpdatedWorkspace,fetchedInsertedWorkspace,"ERROR : workspace not updated")
        self.assertEqual(fetchedUpdatedWorkspace,newWorkspace,"ERROR : updated workspace does not match")
        # call DB delete
        deleteById(CommonTest.cursor,initialWorkspace.id)
        # check DB delete
        deletedWorkspace = selectById(CommonTest.cursor,initialWorkspace.id)
        self.assertIsNone(deletedWorkspace,"ERROR : workspace not deleted")
        pass
    # test select/delete all OK
    def testSelectDeleteAll(self):
        # initialize random workspaces
        initialIds=insertRandomWorkspaces()
        # select IDs
        fetchedIds = selectAllIds(CommonTest.cursor)
        # check IDs
        self.assertTrue(initialIds.issubset(fetchedIds),"ERROR : IDs selection does not match")
        ''' INFO : we do not check the "delete all" functionnality in order to avoid :
         - delete other people workspace
         - error with existing workspace containing perceptrons or training sets'''
        # delete all
        for initialId in initialIds:
            deleteById(CommonTest.cursor,initialId)
        # check IDs
        remainingIds = set(selectAllIds(CommonTest.cursor))
        self.assertNotIn(initialIds,remainingIds, "ERROR : IDs selection does not match")
        pass
    # test workspace error
    # test error
    # INFO : unable to produce
    '''
    from neuralnetworkcommon.entity.workspace import Workspace
    from testpythoncommontools.database.database import checkPostgreSqlDatabaseError
    badWorkspace = Workspace.__init__('')
    def testWorkspaceInsertError(self):
        checkPostgreSqlDatabaseError(self, insert, (badWorkspace,))
    def testWorkspaceUpdateError(self):
        checkPostgreSqlDatabaseError(self, update, (badWorkspace,))
    '''
    pass
pass
