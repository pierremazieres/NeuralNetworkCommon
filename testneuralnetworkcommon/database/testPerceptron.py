#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworkcommon.database.perceptron import insert, selectById, update, selectSummaryById, patch, deleteById, selectAllIdsByWorkspaceId, deleteAllByWorkspaceId
from neuralnetworkcommon.database.layer import deleteAllByPerceptronId
from neuralnetworkcommon.entity.perceptron.perceptron import Perceptron
from random import choice
from string import ascii_letters
from testneuralnetworkcommon.commonUtilities import CommonTest, randomPerceptron, insertRandomPerceptrons, insertRandomLayers
from testpythoncommontools.database.database import checkPostgreSqlDatabaseError
# test perceptron
badPerceptron = Perceptron('')
class testPerceptronDB(CommonTest):
    # test CRUD OK
    def testCrudOK(self):
        # initialize random perceptron
        initialPerceptron = randomPerceptron()
        # precheck
        self.assertIsNone(initialPerceptron.id,"ERROR : perceptron has id")
        # call DB insert
        insert(CommonTest.cursor,initialPerceptron)
        # check DB insert
        self.assertTrue(hasattr(initialPerceptron,"id"),"ERROR : perceptron has no id")
        # call DB select by id
        fetchedInsertedPerceptron = selectById(CommonTest.cursor,initialPerceptron.id)
        # check DB select by id
        self.assertEqual(initialPerceptron,fetchedInsertedPerceptron,"ERROR : inserted perceptron does not match")
        # call DB update
        updatedPerceptron = randomPerceptron(initialPerceptron.id)
        update(CommonTest.cursor,updatedPerceptron)
        # check DB update
        fetchedUpdatedPerceptron = selectById(CommonTest.cursor,initialPerceptron.id)
        self.assertNotEqual(fetchedUpdatedPerceptron,fetchedInsertedPerceptron,"ERROR : perceptron not updated")
        self.assertEqual(fetchedUpdatedPerceptron,updatedPerceptron,"ERROR : updated perceptron does not match")
        # call DB summary
        layersNumber = insertRandomLayers(initialPerceptron.id)
        perceptronSummary = selectSummaryById(CommonTest.cursor,initialPerceptron.id)
        # check DB summary
        self.assertEqual(perceptronSummary.id,fetchedUpdatedPerceptron.id,"ERROR : perceptron summary id does not match")
        self.assertEqual(perceptronSummary.workspaceId,CommonTest.testWorkspaceId,"ERROR : perceptron summary workspaceId does not match")
        self.assertEqual(perceptronSummary.comments,fetchedUpdatedPerceptron.comments,"ERROR : perceptron summary comments does not match")
        self.assertEqual(perceptronSummary.layersNumber,layersNumber,"ERROR : perceptron summary layers does not match")
        # call DB update comments
        updatedComments = ''.join([choice(ascii_letters) for _ in range(50)])
        updatedPerceptronPatch = Perceptron(initialPerceptron.id,CommonTest.testWorkspaceId,updatedComments)
        patch(CommonTest.cursor,updatedPerceptronPatch)
        # check DB update comments
        fetchedUpdatedCommentsPerceptron = selectById(CommonTest.cursor,initialPerceptron.id)
        deleteAllByPerceptronId(CommonTest.cursor,initialPerceptron.id)
        self.assertNotEqual(fetchedUpdatedCommentsPerceptron.comments,fetchedUpdatedPerceptron.comments,"ERROR : perceptron comments not updated")
        self.assertEqual(fetchedUpdatedCommentsPerceptron.comments,updatedComments,"ERROR : perceptron comments does not match")
        # call DB delete
        deleteById(CommonTest.cursor,initialPerceptron.id)
        # check DB delete
        deletedPerceptron = selectById(CommonTest.cursor,initialPerceptron.id)
        self.assertIsNone(deletedPerceptron,"ERROR : perceptron not deleted")
        pass
    # test select/delete all OK
    def testSelectDeleteAll(self):
        # initialize random perceptrons
        perceptronIds=insertRandomPerceptrons()
        # select IDs
        fetchedIds = selectAllIdsByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        # check IDs
        self.assertTrue(perceptronIds.issubset(fetchedIds),"ERROR : IDs selection does not match")
        # delete all
        deleteAllByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        # check IDs
        deletedIds = selectAllIdsByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        self.assertEqual(len(deletedIds),0,"ERROR : complete deletion failed")
        pass
    # test perceptron error
    def testPerceptronInsertError(self):
        checkPostgreSqlDatabaseError(self, insert, (CommonTest.cursor,badPerceptron,))
    def testPerceptronUpdateError(self):
        checkPostgreSqlDatabaseError(self, update, (CommonTest.cursor,badPerceptron,))
    def testPerceptronUpdateWorkspaceIdCommentsError(self):
        checkPostgreSqlDatabaseError(self, patch, (CommonTest.cursor,badPerceptron,))
    pass
pass
