#!/usr/bin/env python3
# coding=utf-8
# import
from testneuralnetworkcommon.commonUtilities import CommonTest, randomLayer, insertRandomLayers
from neuralnetworkcommon.database.layer import insert, selectByPerceptronIdAndDepthIndex, selectSummaryByPerceptronIdAndDepthIndex, update, deleteByPerceptronIdAndDepthIndex, selectAllIdsByPerceptronId, deleteAllByPerceptronId
from testpythoncommontools.database.database import checkPostgreSqlDatabaseError
from neuralnetworkcommon.entity.layer.layer import Layer
from random import randint
# test layer
badLayer = Layer('',0,[[]],[])
class testLayerDB(CommonTest):
    # test CRUD OK
    def testCrudOK(self):
        # initialize random data
        depthIndex = randint(0,5)
        initialLayer = randomLayer(CommonTest.testPerceptronId,depthIndex)
        initialPreviousDimension = randint(2,5)
        initialCurrentDimension = randint(3,9)
        # call DB insert
        insert(CommonTest.cursor,initialLayer, initialPreviousDimension, initialCurrentDimension)
        # call DB select by id
        fetchedInsertedLayer = selectByPerceptronIdAndDepthIndex(CommonTest.cursor,CommonTest.testPerceptronId, depthIndex)
        # check DB select by id
        self.assertEqual(fetchedInsertedLayer.perceptronId,CommonTest.testPerceptronId,"ERROR : inserted layer perceptronId does not match")
        self.assertEqual(fetchedInsertedLayer.depthIndex,depthIndex,"ERROR : inserted layer depthIndex does not match")
        self.assertEqual(len(fetchedInsertedLayer.weights),initialCurrentDimension,"ERROR : inserted layer weights row length does not match")
        self.assertEqual(len(fetchedInsertedLayer.weights[0]),initialPreviousDimension,"ERROR : inserted layer weights column length does not match")
        self.assertEqual(len(fetchedInsertedLayer.biases),initialCurrentDimension,"ERROR : inserted layer biases length does not match")
        # initialize random data
        updaptedLayer = randomLayer(CommonTest.testPerceptronId,depthIndex,randint(10,15), randint(13,19))
        # call DB summary
        layerSummary = selectSummaryByPerceptronIdAndDepthIndex(CommonTest.cursor,CommonTest.testPerceptronId, depthIndex)
        # check DB summary
        self.assertEqual(layerSummary.perceptronId,CommonTest.testPerceptronId,"ERROR : layer summary perceptronId does not match")
        self.assertEqual(layerSummary.depthIndex,depthIndex,"ERROR : layer summary depthIndex does not match")
        self.assertEqual(layerSummary.weightsDimensions,[initialCurrentDimension,initialPreviousDimension],"ERROR : layer summary weightsDimensions does not match")
        self.assertEqual(layerSummary.biasesDimension,initialCurrentDimension,"ERROR : layer summary initialCurrentDimension does not match")
        # call DB update
        update(CommonTest.cursor,updaptedLayer)
        # check DB update
        fetchedUpdaptedLayer = selectByPerceptronIdAndDepthIndex(CommonTest.cursor,CommonTest.testPerceptronId, depthIndex)
        self.assertNotEqual(fetchedUpdaptedLayer,fetchedInsertedLayer,"ERROR : layer not updated")
        self.assertEqual(fetchedUpdaptedLayer,updaptedLayer,"ERROR : updapted layer does not match")
        # call DB delete
        deleteByPerceptronIdAndDepthIndex(CommonTest.cursor,CommonTest.testPerceptronId, depthIndex)
        # check DB delete
        deletedLayer = selectByPerceptronIdAndDepthIndex(CommonTest.cursor,CommonTest.testPerceptronId, depthIndex)
        self.assertIsNone(deletedLayer,"ERROR : layer not deleted")
        pass
    # test select/delete all OK
    def testSelectDeleteAll(self):
        # initialize random layers
        layerNumber=insertRandomLayers(CommonTest.testPerceptronId)
        # select IDs
        fetchedIds = selectAllIdsByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        # check IDs
        self.assertGreaterEqual(len(fetchedIds),layerNumber,"ERROR : IDs selection does not match")
        # delete all
        deleteAllByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        # check IDs
        deletedIds = selectAllIdsByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertEqual(len(deletedIds),0,"ERROR : complete deletion failed")
        pass
    # test layer error
    def testLayerInsertError(self):
        checkPostgreSqlDatabaseError(self, insert, (CommonTest.cursor,badLayer,0,0,))
    def testLayerUpdateError(self):
        checkPostgreSqlDatabaseError(self, update, (CommonTest.cursor,badLayer,))
    pass
pass
