#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworkcommon.database.perceptron import deleteAllByWorkspaceId
from neuralnetworkcommon.database.trainingSessionProgress import insert,selectByPerceptronId,update,deleteByPerceptronId,initialize,append,selectAllIdsByWorkspaceId,deleteAllByWorkspaceId
from random import random, randint
from testneuralnetworkcommon.commonUtilities import CommonTest, randomTrainingSessionProgress, insertRandomTrainingSessionProgresses
from neuralnetworkcommon.entity.trainingSessionProgress import TrainingSessionProgress
from testpythoncommontools.database.database import checkPostgreSqlDatabaseError
# test training session
badTrainingSessionProgress = TrainingSessionProgress('',None,None,None)
class testTrainingSessionDB(CommonTest):
    # test CRUD OK
    def testCrudOK(self):
        # initialize random training session progress
        initialTrainingSessionProgress = randomTrainingSessionProgress(CommonTest.testPerceptronId)
        # call DB insert
        insert(CommonTest.cursor,initialTrainingSessionProgress)
        # call DB select by id
        fetchedInsertedTrainingSessionProgress = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        # check DB select by id
        self.assertEqual(initialTrainingSessionProgress,fetchedInsertedTrainingSessionProgress,"ERROR : inserted trainingSession does not match")
        # call DB update training progress
        updatedTrainingSessionProgress = randomTrainingSessionProgress(CommonTest.testPerceptronId)
        update(CommonTest.cursor,updatedTrainingSessionProgress)
        # check DB update
        fetchedUpdatedTrainingSessionProgress = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertNotEqual(initialTrainingSessionProgress,fetchedUpdatedTrainingSessionProgress,"ERROR : trainingSessionProgress not updated")
        self.assertEqual(updatedTrainingSessionProgress,fetchedUpdatedTrainingSessionProgress,"ERROR : updated trainingSessionProgress does not match")
        # call DB delete
        deleteByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        # check DB delete
        deletedTrainingSessionProgress = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNone(deletedTrainingSessionProgress,"ERROR : trainingSessionProgress not deleted")
        # call DB initialize
        initialMeanDifferentialError = random()
        initialErrorElementsNumber = randint(101, 200)
        initialize(CommonTest.cursor,CommonTest.testPerceptronId, initialMeanDifferentialError, initialErrorElementsNumber,False)
        # check initialize
        fetchedAppendedTrainingSessionProgress = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertEqual(fetchedAppendedTrainingSessionProgress.meanDifferentialErrors,[initialMeanDifferentialError],"ERROR : appended meanDifferentialError does not match")
        self.assertEqual(fetchedAppendedTrainingSessionProgress.errorElementsNumbers,[initialErrorElementsNumber],"ERROR : appended errorElementsNumber does not match")
        self.assertEqual(fetchedAppendedTrainingSessionProgress.resets,[False],"ERROR : appended reset does not match")
        # call DB append
        appendMeanDifferentialError = random()
        appendErrorElementsNumber = randint(101, 200)
        append(CommonTest.cursor,CommonTest.testPerceptronId, appendMeanDifferentialError, appendErrorElementsNumber, True)
        # check append
        fetchedAppendedTrainingSessionProgress = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertEqual(fetchedAppendedTrainingSessionProgress.meanDifferentialErrors,[initialMeanDifferentialError,appendMeanDifferentialError],"ERROR : appended meanDifferentialError does not match")
        self.assertEqual(fetchedAppendedTrainingSessionProgress.errorElementsNumbers,[initialErrorElementsNumber,appendErrorElementsNumber],"ERROR : appended errorElementsNumber does not match")
        self.assertEqual(fetchedAppendedTrainingSessionProgress.resets,[False,True],"ERROR : appended reset does not match")
        pass
    # test select/delete all OK
    def testSelectDeleteAll(self):
        # initialize random training sessions
        perceptronIds = insertRandomTrainingSessionProgresses()
        # select IDs
        fetchedIds = selectAllIdsByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        # check IDs
        self.assertTrue(perceptronIds.issubset(fetchedIds),"ERROR : IDs selection does not match")
        # delete all
        deleteAllByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        # check IDs
        deletedIds = selectAllIdsByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        self.assertEqual(len(deletedIds),0,"ERROR : complete deletion failed")
        # clean database
        deleteAllByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        pass
    # test error
    def testTrainingSessionInsertError(self):
        checkPostgreSqlDatabaseError(self, insert, (CommonTest.cursor,badTrainingSessionProgress,))
    def testTrainingSessionUpdateError(self):
        checkPostgreSqlDatabaseError(self, update, (CommonTest.cursor,badTrainingSessionProgress,))
    pass
pass
