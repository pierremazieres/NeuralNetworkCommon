#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworkcommon.database.trainingSet import insert,selectById,update,selectSummaryById,patch,deleteById,selectAllIdsByWorkspaceId,deleteAllByWorkspaceId
from neuralnetworkcommon.database.trainingElement import deleteAllByTrainingSetId
from neuralnetworkcommon.entity.trainingSet.trainingSet import TrainingSet
from random import choice
from testneuralnetworkcommon.commonUtilities import CommonTest, insertRandomTrainingElements, randomTrainingSet, insertRandomTrainingSets
from string import ascii_letters
from testpythoncommontools.database.database import checkPostgreSqlDatabaseError
# test training set
badTrainingSet = TrainingSet('', -1)
class testTrainingSetDB(CommonTest):
    # test CRUD OK
    def testCrudOK(self):
        # initialize random trainingSet
        initialTrainingSet = randomTrainingSet()
        # precheck
        self.assertIsNone(initialTrainingSet.id,"ERROR : trainingSet has id")
        # call DB insert
        comments = ''.join([choice(ascii_letters) for _ in range(15)])
        initialTrainingSet.comments = comments
        insert(CommonTest.cursor,initialTrainingSet)
        # check DB insert
        trainingSetId = initialTrainingSet.id
        self.assertIsNotNone(trainingSetId,"ERROR : trainingSet has no id")
        # call DB select by id
        fetchedInsertedTrainingSet = selectById(CommonTest.cursor,trainingSetId)
        # check DB select by id
        self.assertEqual(initialTrainingSet,fetchedInsertedTrainingSet,"ERROR : inserted trainingSet does not match")
        # call DB update
        newTrainingSet = randomTrainingSet()
        newTrainingSet.id = trainingSetId
        update(CommonTest.cursor,newTrainingSet)
        # check DB update
        fetchedUpdatedTrainingSet = selectById(CommonTest.cursor,trainingSetId)
        self.assertNotEqual(fetchedUpdatedTrainingSet,fetchedInsertedTrainingSet,"ERROR : trainingSet not updated")
        self.assertEqual(fetchedUpdatedTrainingSet,newTrainingSet,"ERROR : updated trainingSet does not match")
        # fill DB summary
        trainingElementNumber = len(insertRandomTrainingElements(trainingSetId))
        # call DB summary
        trainingSetSummary = selectSummaryById(CommonTest.cursor,trainingSetId)
        # check DB summary
        self.assertEqual(trainingSetSummary.id,fetchedUpdatedTrainingSet.id,"ERROR : trainingSet summary id does not match")
        self.assertEqual(trainingSetSummary.workspaceId,fetchedUpdatedTrainingSet.workspaceId,"ERROR : trainingSet summary workspaceId does not match")
        self.assertEqual(trainingSetSummary.comments,fetchedUpdatedTrainingSet.comments,"ERROR : trainingSet summary comments does not match")
        self.assertEqual(trainingElementNumber,trainingSetSummary.trainingElementsNumber,"ERROR : trainingSet summary elements does not match")
        # clean DB summary
        deleteAllByTrainingSetId(CommonTest.cursor,trainingSetId)
        # call DB update comments
        updatedComments = ''.join([choice(ascii_letters) for _ in range(50)])
        updatedTrainingSetPatch = TrainingSet(trainingSetId,initialTrainingSet.workspaceId,updatedComments)
        patch(CommonTest.cursor,updatedTrainingSetPatch)
        # check DB update comments
        fetchedUpdatedCommentsTrainingSet = selectById(CommonTest.cursor,trainingSetId)
        self.assertNotEqual(fetchedUpdatedCommentsTrainingSet.comments,fetchedUpdatedTrainingSet.comments,"ERROR : trainingSet comments not updated")
        self.assertEqual(fetchedUpdatedCommentsTrainingSet.comments,updatedComments,"ERROR : trainingSet comments does not match")
        # call DB delete
        deleteById(CommonTest.cursor,trainingSetId)
        # check DB delete
        deletedTrainingSet = selectById(CommonTest.cursor,trainingSetId)
        self.assertIsNone(deletedTrainingSet,"ERROR : trainingSet not deleted")
        pass
    # test select/delete all OK
    def testSelectDeleteAll(self):
        # initialize random training sets
        trainingSetsIds=insertRandomTrainingSets()
        # select IDs
        fetchedIds = selectAllIdsByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        # check IDs
        self.assertTrue(trainingSetsIds.issubset(fetchedIds),"ERROR : IDs selection does not match")
        # delete all
        deleteAllByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        # check IDs
        deletedIds = selectAllIdsByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        self.assertEqual(len(deletedIds),0,"ERROR : complete deletion failed")
        pass
    # test training set error
    def testTrainingSetInsertError(self):
        checkPostgreSqlDatabaseError(self, insert, (CommonTest.cursor,badTrainingSet,))
    def testTrainingSetUpdateError(self):
        checkPostgreSqlDatabaseError(self, update, (CommonTest.cursor,badTrainingSet,))
    def testTrainingSetUpdateWorkspaceIdCommentsError(self):
        checkPostgreSqlDatabaseError(self, patch, (CommonTest.cursor,badTrainingSet,))
    pass
pass
