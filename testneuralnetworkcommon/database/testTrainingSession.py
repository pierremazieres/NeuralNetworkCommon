#!/usr/bin/env python3
# coding=utf-8
# import
from neuralnetworkcommon.database.perceptron import deleteById as perceptronDeleteById, deleteAllByWorkspaceId as perceptronDeleteAllByWorkspaceId
from neuralnetworkcommon.database.trainingSession import insert,selectByPerceptronId,updateTestScore,updatePid,updateError,update,selectSummaryByPerceptronId,deleteByPerceptronId,selectAllIdsByWorkspaceId,deleteAllByWorkspaceId,patch
from neuralnetworkcommon.database.trainingSessionProgress import insert as trainingSessionProgressInsert, selectByPerceptronId as trainingSessionProgressSelectByPerceptronId, deleteByPerceptronId as trainingSessionProgressDeleteByPerceptronId
from neuralnetworkcommon.database.trainingSessionElement import selectAllByPerceptronIdAndFilter
from neuralnetworkcommon.database.trainingSet import deleteById as trainingSetDeleteById, deleteAllByWorkspaceId as trainingSetDeleteAllByWorkspaceId
from neuralnetworkcommon.database.trainingElement import selectAllByTrainingSetIdAndFilter,selectById,deleteAllByTrainingSetId
from random import random, randint, choice, uniform
from string import ascii_letters
from testneuralnetworkcommon.commonUtilities import CommonTest, insertRandomTrainingElements, insertRandomTrainingSessions, randomTrainingSession, randomTrainingSessionProgress, randomizeTrainingSessionElementsError
from neuralnetworkcommon.entity.trainingSession.trainingSession import TrainingSession
from testpythoncommontools.database.database import checkPostgreSqlDatabaseError
# test training session
badTrainingSession = TrainingSession('','',None,None,None,None,None,None,None)
class testTrainingSessionDB(CommonTest):
    # test CRUD OK
    def testCrudOK(self):
        # initialize random training session
        initialTrainingSession = randomTrainingSession(CommonTest.testPerceptronId,CommonTest.testTrainingSetId,None,None,None)
        elementsNumber = len(insertRandomTrainingElements(CommonTest.testTrainingSetId))
        # call DB insert
        initialTestRatio = random()
        insert(CommonTest.cursor,initialTrainingSession,initialTestRatio)
        randomizeTrainingSessionElementsError(CommonTest.testPerceptronId)
        # call DB select by id
        fetchedInsertedTrainingSession = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        # check inserted training / test dispatch
        # INFO : use set to compare without order
        inserted_elementsIds = selectAllByTrainingSetIdAndFilter(CommonTest.cursor,CommonTest.testTrainingSetId)
        inserted_allIdsByTrainingSetId = set(inserted_elementsIds)
        inserted_allIdsByPerceptronId = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId))
        inserted_allIdsByPerceptronId_errorTraining = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=False,errorFilter=False));
        inserted_allIdsByPerceptronId_fineTraining = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=False,errorFilter=True));
        inserted_allIdsByPerceptronId_errorTest = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=True,errorFilter=False));
        inserted_allIdsByPerceptronId_fineTest = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=True,errorFilter=True));
        inserted_allIdsByPerceptronId_training = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=False));
        inserted_allIdsByPerceptronId_test = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=True));
        inserted_allIdsByPerceptronId_error = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,errorFilter=False));
        inserted_allIdsByPerceptronId_fine = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,errorFilter=True));
        inserted_firstElementId = inserted_elementsIds[0]
        inserted_firstElement = selectById(CommonTest.cursor,inserted_firstElementId)
        inserted_allIdsByPerceptronId_0 = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,expectedOutput=inserted_firstElement.expectedOutput));
        delta = .1
        self.assertAlmostEqual(len(inserted_allIdsByPerceptronId_test) / elementsNumber, initialTestRatio, delta=delta, msg="ERROR : inserted training / test dispatch does not match")
        self.assertSetEqual(inserted_allIdsByPerceptronId, inserted_allIdsByTrainingSetId, msg="ERROR : inserted IDs by perceptron vs. training set does not match")
        self.assertSetEqual(inserted_allIdsByPerceptronId_errorTraining.union(inserted_allIdsByPerceptronId_fineTraining), inserted_allIdsByPerceptronId_training, msg="ERROR : inserted training IDs does not match")
        self.assertSetEqual(inserted_allIdsByPerceptronId_errorTest.union(inserted_allIdsByPerceptronId_fineTest), inserted_allIdsByPerceptronId_test, msg="ERROR : inserted test IDs does not match")
        self.assertSetEqual(inserted_allIdsByPerceptronId_errorTraining.union(inserted_allIdsByPerceptronId_errorTest), inserted_allIdsByPerceptronId_error, msg="ERROR : inserted error IDs does not match")
        self.assertSetEqual(inserted_allIdsByPerceptronId_fineTraining.union(inserted_allIdsByPerceptronId_fineTest), inserted_allIdsByPerceptronId_fine, msg="ERROR : inserted fine IDs does not match")
        self.assertSetEqual(inserted_allIdsByPerceptronId_0,{inserted_firstElementId}, msg="ERROR : inserted first ID does not match")
        # check DB select by id
        self.assertEqual(initialTrainingSession,fetchedInsertedTrainingSession,"ERROR : inserted trainingSession does not match")
        # call DB update test score
        initialTestScore = random()
        updateTestScore(CommonTest.cursor,CommonTest.testPerceptronId, initialTestScore)
        # check DB update test score
        fetchedUpdatedTestScore = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertEqual(fetchedUpdatedTestScore.testScore,initialTestScore,"ERROR : testScore does not match")
        # call DB update filled PID
        initialPid = randint(1,1e3)
        updatePid(CommonTest.cursor,CommonTest.testPerceptronId,initialPid)
        # check DB update filled PID
        fetchedUpdatedFilledPid = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertEqual(fetchedUpdatedFilledPid.pid,initialPid,"ERROR : filled updatePid PID does not match")
        self.assertIsNone(fetchedUpdatedFilledPid.errorMessage,"ERROR : filled updatePid errorMessage does not match")
        # call DB update none PID
        updatePid(CommonTest.cursor,CommonTest.testPerceptronId,None)
        # check DB update none PID
        fetchedUpdatedNonePid = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNone(fetchedUpdatedNonePid.pid,"ERROR : none updatePid PID does not match")
        self.assertIsNone(fetchedUpdatedNonePid.errorMessage,"ERROR : none updatePid errorMessage does not match")
        # call DB update error
        initialErrorMessage = ''.join([choice(ascii_letters) for _ in range(50)])
        updateError(CommonTest.cursor,CommonTest.testPerceptronId,initialErrorMessage)
        # check DB update error
        fetchedUpdatedError = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNone(fetchedUpdatedError.pid,"ERROR : PID does not match")
        self.assertEqual(fetchedUpdatedError.errorMessage,initialErrorMessage,"ERROR : errorMessage does not match")
        # insert training progress
        trainingSessionProgress = randomTrainingSessionProgress(CommonTest.testPerceptronId,randint(1000,9999))
        trainingSessionProgressInsert(CommonTest.cursor,trainingSessionProgress)
        insertedTrainingSessionProgress = trainingSessionProgressSelectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNotNone(insertedTrainingSessionProgress,"ERROR : trainingSessionProgress not inserted")
        # call DB update all
        updatedTrainingSession = randomTrainingSession(CommonTest.testPerceptronId,CommonTest.testTrainingSetId)
        updatedTestRatio = random()
        update(CommonTest.cursor,updatedTrainingSession, updatedTestRatio)
        randomizeTrainingSessionElementsError(CommonTest.testPerceptronId)
        # check updated training / test dispatch
        # INFO : use set to compare without order
        updated_elementsIds = selectAllByTrainingSetIdAndFilter(CommonTest.cursor,CommonTest.testTrainingSetId)
        updated_allIdsByTrainingSetId = set(updated_elementsIds)
        updated_allIdsByPerceptronId = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId))
        updated_allIdsByPerceptronId_fineTraining = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=False,errorFilter=False));
        updated_allIdsByPerceptronId_errorTraining = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=False,errorFilter=True));
        updated_allIdsByPerceptronId_fineTest = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=True,errorFilter=False));
        updated_allIdsByPerceptronId_errorTest = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=True,errorFilter=True));
        updated_allIdsByPerceptronId_training = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=False));
        updated_allIdsByPerceptronId_test = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,testFilter=True));
        updated_allIdsByPerceptronId_fine = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,errorFilter=False));
        updated_allIdsByPerceptronId_error = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,errorFilter=True));
        updated_firstElementId = updated_elementsIds[0]
        updated_firstElement = selectById(CommonTest.cursor,updated_firstElementId)
        updated_allIdsByPerceptronId_0 = set(selectAllByPerceptronIdAndFilter(CommonTest.cursor,CommonTest.testPerceptronId,expectedOutput=updated_firstElement.expectedOutput));
        self.assertAlmostEqual(len(updated_allIdsByPerceptronId_test) / elementsNumber, updatedTestRatio, delta=delta, msg="ERROR : updated training / test dispatch does not match")
        self.assertSetEqual(updated_allIdsByPerceptronId, updated_allIdsByTrainingSetId, msg="ERROR : updated IDs by perceptron vs. training set does not match")
        self.assertSetEqual(updated_allIdsByPerceptronId_errorTraining.union(updated_allIdsByPerceptronId_fineTraining), updated_allIdsByPerceptronId_training, msg="ERROR : updated training IDs does not match")
        self.assertSetEqual(updated_allIdsByPerceptronId_errorTest.union(updated_allIdsByPerceptronId_fineTest), updated_allIdsByPerceptronId_test, msg="ERROR : updated test IDs does not match")
        self.assertSetEqual(updated_allIdsByPerceptronId_errorTraining.union(updated_allIdsByPerceptronId_errorTest), updated_allIdsByPerceptronId_error, msg="ERROR : updated error IDs does not match")
        self.assertSetEqual(updated_allIdsByPerceptronId_fineTraining.union(updated_allIdsByPerceptronId_fineTest), updated_allIdsByPerceptronId_fine, msg="ERROR : updated fine IDs does not match")
        self.assertSetEqual(updated_allIdsByPerceptronId_0,{updated_firstElementId}, msg="ERROR : updated first ID does not match")
        # check DB update all
        fetchedUpdatedTrainingSession = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertNotEqual(initialTrainingSession,fetchedUpdatedTrainingSession,"ERROR : trainingSession not updated")
        self.assertEqual(updatedTrainingSession,fetchedUpdatedTrainingSession,"ERROR : updated trainingSession does not match")
        updatedTrainingSessionProgress = trainingSessionProgressSelectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNone(updatedTrainingSessionProgress,"ERROR : trainingSessionProgress not deleted")
        # re-insert training progress
        trainingSessionProgress = randomTrainingSessionProgress(CommonTest.testPerceptronId,randint(1000,9999))
        trainingSessionProgressInsert(CommonTest.cursor,trainingSessionProgress)
        # call DB summary
        trainingSessionSummary = selectSummaryByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        # check DB summary
        saveInterval = fetchedUpdatedTrainingSession.saveInterval
        fetchedTrainingSessionProgress = trainingSessionProgressSelectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertEqual(trainingSessionSummary.perceptronId,fetchedUpdatedTrainingSession.perceptronId,"ERROR : trainingSession summary perceptronId does not match")
        self.assertEqual(trainingSessionSummary.trainingSetId,fetchedUpdatedTrainingSession.trainingSetId,"ERROR : trainingSession summary trainingSetId does not match")
        self.assertEqual(trainingSessionSummary.saveInterval,saveInterval,"ERROR : trainingSession summary saveInterval does not match")
        self.assertEqual(trainingSessionSummary.maximumTry,fetchedUpdatedTrainingSession.maximumTry,"ERROR : trainingSession summary maximumTry does not match")
        self.assertEqual(trainingSessionSummary.maximumErrorRatio,fetchedUpdatedTrainingSession.maximumErrorRatio,"ERROR : maximumErrorRatio summary maximumTry does not match")
        self.assertEqual(trainingSessionSummary.elementsNumber.fineTraining,len(updated_allIdsByPerceptronId_fineTraining),"ERROR : fine trainingSession summary trainingSetsNumber does not match")
        self.assertEqual(trainingSessionSummary.elementsNumber.errorTraining,len(updated_allIdsByPerceptronId_errorTraining),"ERROR : error trainingSession summary testSetsNumber does not match")
        self.assertEqual(trainingSessionSummary.elementsNumber.fineTest,len(updated_allIdsByPerceptronId_fineTest),"ERROR : fine trainingSession summary trainingSetsNumber does not match")
        self.assertEqual(trainingSessionSummary.elementsNumber.errorTest,len(updated_allIdsByPerceptronId_errorTest),"ERROR : error trainingSession summary testSetsNumber does not match")
        self.assertEqual(trainingSessionSummary.pid,fetchedUpdatedTrainingSession.pid,"ERROR : trainingSession summary pid does not match")
        self.assertEqual(trainingSessionSummary.errorMessage,fetchedUpdatedTrainingSession.errorMessage,"ERROR : trainingSession summary errorMessage does not match")
        self.assertEqual(trainingSessionSummary.progressRecordsNumber,len(fetchedTrainingSessionProgress.meanDifferentialErrors),"ERROR : trainingSession summary progressRecordsNumber does not match")
        self.assertEqual(trainingSessionSummary.meanDifferentialErrors,fetchedTrainingSessionProgress.meanDifferentialErrors[-1],"ERROR : trainingSession summary meanDifferentialError does not match")
        self.assertEqual(trainingSessionSummary.errorElementsNumbers,fetchedTrainingSessionProgress.errorElementsNumbers[-1],"ERROR : trainingSession summary errorElementsNumber does not match")
        self.assertEqual(trainingSessionSummary.resets,set(fetchedTrainingSessionProgress.resets).pop(),"ERROR : trainingSession summary resets does not match")
        self.assertEqual(trainingSessionSummary.testScore,fetchedUpdatedTrainingSession.testScore,"ERROR : trainingSession summary testScore does not match")
        self.assertEqual(trainingSessionSummary.comments,fetchedUpdatedTrainingSession.comments,"ERROR : trainingSession summary comments does not match")
        # call DB update training chunk size + save interval + comments
        reupdatedSaveInterval = int(saveInterval * uniform(2,10))
        reupdatedMaximumTry = randint(11,20)
        reupdatedTrainingMaximumErrorRatio = random()
        reupdatedComments = ''.join([choice(ascii_letters) for _ in range(16)])
        trainingSessionPatch = TrainingSession(CommonTest.testPerceptronId,saveInterval=reupdatedSaveInterval,maximumTry=reupdatedMaximumTry,maximumErrorRatio=reupdatedTrainingMaximumErrorRatio,comments=reupdatedComments)
        patch(CommonTest.cursor,trainingSessionPatch)
        fetchedReupdatedTrainingSessionProgress = trainingSessionProgressSelectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        # check DB update training chunk size + save interval + comments
        fetchedReupdatedTrainingSession = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        expectedShrinkRatio = reupdatedSaveInterval / saveInterval
        self.assertNotEqual(fetchedReupdatedTrainingSession.saveInterval,fetchedUpdatedTrainingSession.saveInterval,"ERROR : reupdated saveInterval not updated")
        self.assertEqual(fetchedReupdatedTrainingSession.saveInterval,reupdatedSaveInterval,"ERROR : reupdated saveInterval does not match")
        self.assertNotEqual(fetchedReupdatedTrainingSession.maximumTry,fetchedUpdatedTrainingSession.maximumTry,"ERROR : reupdated maximumTry not updated")
        self.assertEqual(fetchedReupdatedTrainingSession.maximumTry,reupdatedMaximumTry,"ERROR : reupdated maximumTry does not match")
        self.assertNotEqual(fetchedReupdatedTrainingSession.maximumErrorRatio,fetchedUpdatedTrainingSession.maximumErrorRatio,"ERROR : reupdated maximumTry not updated")
        self.assertEqual(fetchedReupdatedTrainingSession.maximumErrorRatio,reupdatedTrainingMaximumErrorRatio,"ERROR : reupdated maximumErrorRatio does not match")
        self.assertNotEqual(fetchedReupdatedTrainingSession.comments,fetchedUpdatedTrainingSession.comments,"ERROR : reupdated comments not updated")
        self.assertEqual(fetchedReupdatedTrainingSession.comments,reupdatedComments,"ERROR : reupdated comments does not match")
        self.assertAlmostEqual(len(fetchedTrainingSessionProgress.meanDifferentialErrors)/len(fetchedReupdatedTrainingSessionProgress.meanDifferentialErrors),expectedShrinkRatio,delta=delta, msg="ERROR : reupdated meanDifferentialErrors ratio does not match")
        self.assertAlmostEqual(len(fetchedTrainingSessionProgress.errorElementsNumbers)/len(fetchedReupdatedTrainingSessionProgress.errorElementsNumbers),expectedShrinkRatio,delta=delta, msg="ERROR : reupdated errorElementsNumbers ratio does not match")
        self.assertAlmostEqual(len(fetchedTrainingSessionProgress.resets)/len(fetchedReupdatedTrainingSessionProgress.resets),expectedShrinkRatio,delta=delta, msg="ERROR : reupdated resets ratio does not match")
        self.assertEqual(fetchedReupdatedTrainingSession.pid,fetchedUpdatedTrainingSession.pid,"ERROR : reupdated pid does not match")
        self.assertEqual(fetchedReupdatedTrainingSession.errorMessage,fetchedUpdatedTrainingSession.errorMessage,"ERROR : reupdated errorMessage does not match")
        self.assertEqual(fetchedReupdatedTrainingSession.testScore,fetchedUpdatedTrainingSession.testScore,"ERROR : reupdated testScore does not match")
        # call DB delete
        deleteByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        # check DB delete
        deletedTrainingSession = selectByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        self.assertIsNone(deletedTrainingSession,"ERROR : trainingSession not deleted")
        # clean database
        trainingSessionProgressDeleteByPerceptronId(CommonTest.cursor,CommonTest.testPerceptronId)
        perceptronDeleteById(CommonTest.cursor,CommonTest.testPerceptronId)
        deleteAllByTrainingSetId(CommonTest.cursor,CommonTest.testTrainingSetId)
        trainingSetDeleteById(CommonTest.cursor,CommonTest.testTrainingSetId)
        pass
    # test select/delete all OK
    def testSelectDeleteAll(self):
        # initialize random training sessions
        perceptronIds = insertRandomTrainingSessions()
        # select IDs
        fetchedIds = selectAllIdsByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        # check IDs
        self.assertTrue(perceptronIds.issubset(fetchedIds),"ERROR : IDs selection does not match")
        # delete all
        deleteAllByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        # check IDs
        deletedIds = selectAllIdsByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        self.assertEqual(len(deletedIds),0,"ERROR : complete deletion failed")
        # clean database
        perceptronDeleteAllByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        trainingSetDeleteAllByWorkspaceId(CommonTest.cursor,CommonTest.testWorkspaceId)
        pass
    # test error
    def testTrainingSessionInsertError(self):
        checkPostgreSqlDatabaseError(self, insert, (CommonTest.cursor,badTrainingSession,0))
    def testTrainingSessionUpdateError(self):
        checkPostgreSqlDatabaseError(self, update, (CommonTest.cursor,badTrainingSession,0))
    def testTrainingSessionUpdateTrainingParametersError(self):
        checkPostgreSqlDatabaseError(self, patch, (CommonTest.cursor,badTrainingSession,))
        CommonTest.connection.commit()
    pass
pass
