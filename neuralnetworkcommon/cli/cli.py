# coding=utf-8
# import
from argparse import ArgumentParser
from enum import Enum, unique
from pythoncommontools.cli.cli import executeCommandCheckResponse, decodeReponse, writeToFile, readFromFile
from requests import post, get, put, patch as patchDefault, delete as deleteDefault
# contants
@unique
class ActionType(Enum):
    CREATE="create"
    READ="read"
    UPDATE="update"
    DELETE="delete"
    SUMMARIZE="summarize"
    PATCH="patch"
    EXECUTE="execute"
# CLI parameter
class ShortCode(Enum):
    ID="-i"
    RELATED_ID="-r"
    SOURCE_FILE="-s"
    TARGET_FILE="-t"
parser = ArgumentParser()
parser.add_argument("action", type=str, choices=[_.value for _ in ActionType], help="action to execute")
parser.add_argument(ShortCode.ID.value, "--id", type=int, help="resource ID, ex. 123")
parser.add_argument(ShortCode.RELATED_ID.value, "--related-id", type=int, help="parent (related) ID, ex. 45")
parser.add_argument(ShortCode.SOURCE_FILE.value, "--source-file", type=str, help="file to read resource, ex. /tmp/source")
parser.add_argument(ShortCode.TARGET_FILE.value, "--target-file", type=str, help="file to write resource, ex. /tmp/target")
# execute command
def executeCommand(command,URL,sourceFile=None,targetFile=None,sendResponse=False):
    data = readFromFile(sourceFile) if sourceFile else None
    response = executeCommandCheckResponse(command,URL, data)
    if targetFile:
        writeToFile(targetFile, response.content)
    if sendResponse:
        response = decodeReponse(response)
        return response
    pass
# create
def create(URL,sourceFile,sendResponse=True):
    response = executeCommand(post, URL, sourceFile=sourceFile, sendResponse=sendResponse)
    return response if sendResponse else None
# read all
def readAll(URL,sendResponse=True):
    response = executeCommand(get, URL, sendResponse=sendResponse)
    return response
# read single
def readSingle(URL,targetFile):
    executeCommand(get, URL, targetFile=targetFile)
    pass
# update
def update(URL,sourceFile):
    executeCommand(put, URL, sourceFile=sourceFile)
    pass
# delete
def delete(URL):
    executeCommand(deleteDefault, URL)
    pass
# patch
def patch(URL,sourceFile):
    executeCommand(patchDefault, URL, sourceFile=sourceFile)
    pass
# summarize
def summarize(URL,targetFile):
    executeCommand(get, URL, targetFile=targetFile)
    pass
# execute
def execute(URL,sourceFile,targetFile):
    executeCommand(put, URL, sourceFile=sourceFile, targetFile=targetFile)
    pass
pass