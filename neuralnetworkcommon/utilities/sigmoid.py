# coding=utf-8
# import
from numpy import exp, array
# sigmoid
# TODO : create an abstract class for all future functions
# TODO : compute with spark each method
# TODO : add extra parameters : uncertainties, dilatation, offsets
def value(variables):
    arrayVariables = array(variables)
    #value = dilatations / (1 + exp(-array(arrayVariables) * uncertainties)) + offsets
    value = 1 / (1 + exp(-arrayVariables))
    return value
# INFO : we compute the derivative from : value = sigmoïd(variables)
def derivative(variables):
    arrayVariables = array(variables)
    #derivative = dilatations * uncertainties * arrayVariables * (1 - arrayVariables)
    derivative = variables * (1 - arrayVariables)
    return derivative
pass