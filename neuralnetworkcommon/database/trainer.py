# coding=utf-8
# import
from neuralnetworkcommon.database.trainingElement import table as trainingElementTable
from neuralnetworkcommon.database.trainingSession import updateTestScore, table as trainingSessionTable
from neuralnetworkcommon.database.trainingSessionElement import table as trainingSessionElementTable
from pythoncommontools.database.postgresql import PostgreSqlUnitaryReadProcessRowTable
from numpy import mean
from neuralnetworkcommon.database.perceptron import selectById
from neuralnetworkcommon.database.database import connectDatabase
# trainer
# INFO : each server cursor must be reset after each end of loop in order to read updated values next time
class TrainerDB(PostgreSqlUnitaryReadProcessRowTable):
    # utilities
    # TODO : create others check methods (not only match maximum element)
    def analysePerceptronTestElement(self, input, expectedOutput):
        expectedOutput = expectedOutput
        maximumExpectedValue = max(expectedOutput)
        expectedIndex = expectedOutput.index(maximumExpectedValue)
        actualOutput = self.perceptron.layerTrainingDB.passForward(input)
        actualOutput = [float(_) for _ in actualOutput]
        maximumActualValue = max(actualOutput)
        actualIndex = actualOutput.index(maximumActualValue)
        outputOK = expectedIndex == actualIndex
        return outputOK
    def resetElementsOnErrors(self,isTest):
        notClause = "" if isTest else "NOT"
        resetErrorStatement = "UPDATE " + trainingSessionElementTable + " SET ON_ERROR=FALSE WHERE PERCEPTRON_ID=%s AND (" + notClause +  " IS_TEST)"
        resetErrorParameters = (self.perceptron.id,)
        self.clientCursor.execute(resetErrorStatement, resetErrorParameters)
    def updateElementsOnErrors(self,errorElementIds):
        errorElementsNumber = len(errorElementIds)
        if errorElementsNumber > 0:
            onErrorStatement = "UPDATE " + trainingSessionElementTable + " SET ON_ERROR=TRUE WHERE TRAINING_SET_ELEMENT_ID IN (" + ','.join(['%s'] * errorElementsNumber) + ')'
            self.clientCursor.execute(onErrorStatement, errorElementIds)
    def getTestElement(self,attributs,inputIndex):
        input = [float(_) for _ in attributs[inputIndex]]
        expectedOutput = [float(_) for _ in attributs[inputIndex+1]]
        return input, expectedOutput
    # training
    def generateTrainingSubSequence(self):
        # initialization
        raisedException = None
        errorElementIds = list()
        errorElementsNumber = 0
        try:
            # reset test elements on errors
            self.resetElementsOnErrors(False)
            # select all training elements
            selectStatement = "DECLARE "+self.serverCursor+" CURSOR FOR SELECT TE.ID,TE.ORIGINAL_INPUT,TE.EXPECTED_OUTPUT FROM " + trainingElementTable + " TE JOIN " + trainingSessionTable + " TS ON TE.TRAINING_SET_ID=TS.TRAINING_SET_ID JOIN " + trainingSessionElementTable + " TSE ON TE.ID=TSE.TRAINING_SET_ELEMENT_ID WHERE TS.PERCEPTRON_ID=%s AND (NOT TSE.IS_TEST)"
            selectParameters = (self.perceptron.id,)
            attributs = self.getFisrstRow(selectStatement, selectParameters)
            # list training elements on error
            # INFO : if memory issue on last update (switch error on true), split this update on many batches
            while attributs:
                input, expectedOutput = self.getTestElement(attributs,1)
                outputOK = self.analysePerceptronTestElement(input, expectedOutput)
                if not outputOK:
                    errorElementIds.append(attributs[0])
                attributs = self.getNextRow()
            self.closeServerCursor()
            # update training elements on errors
            errorElementsNumber = len(errorElementIds)
            self.updateElementsOnErrors(errorElementIds)
            # validate all transactions
            self.cursorConnection.commit()
        except Exception as exception :
            self.cursorConnection.rollback()
            raisedException = exception
        finally:
            if raisedException :
                raise raisedException
        # return presence of error
        return errorElementsNumber
    def passForwardBackwardSequence(self):
        # initialization
        raisedException = None
        differentialErrors = list()
        meanDifferentialError = 0
        try:
            # run forward & backward for each training input / expected output
            selectStatement = "DECLARE " + self.serverCursor + " CURSOR FOR SELECT TE.ORIGINAL_INPUT,TE.EXPECTED_OUTPUT FROM " + trainingElementTable + " TE JOIN " + trainingSessionElementTable + " TSE ON TE.ID=TSE.TRAINING_SET_ELEMENT_ID WHERE (NOT TSE.IS_TEST) AND TSE.ON_ERROR AND TSE.PERCEPTRON_ID=%s ORDER BY RANDOM()"
            selectParameters = (self.perceptron.id,)
            attributs = self.getFisrstRow(selectStatement, selectParameters)
            while attributs:
                input, expectedOutput = self.getTestElement(attributs, 0)
                error = self.perceptron.passForwardBackward(input, expectedOutput)
                # commit layers update (due to pass backward)
                self.extraConnection.commit()
                differentialErrors.append(error)
                meanDifferentialError = mean(differentialErrors)
                attributs = self.getNextRow()
            self.closeServerCursor()
        except Exception as exception :
            self.cursorConnection.rollback()
            raisedException = exception
        finally:
            if raisedException :
                raise raisedException
        # return
        return meanDifferentialError
    def countErrorElements(self):
        # initialization
        raisedException = None
        errorElementsNumber = 0
        try:
            # run over for each training input / expected output
            selectStatement = "DECLARE " + self.serverCursor + " CURSOR FOR SELECT TE.ORIGINAL_INPUT,TE.EXPECTED_OUTPUT FROM " + trainingElementTable + " TE JOIN " + trainingSessionElementTable + " TSE ON TE.ID=TSE.TRAINING_SET_ELEMENT_ID WHERE (NOT TSE.IS_TEST) AND TSE.PERCEPTRON_ID=%s"
            selectParameters = (self.perceptron.id,)
            attributs = self.getFisrstRow(selectStatement, selectParameters)
            while attributs:
                input, expectedOutput = self.getTestElement(attributs, 0)
                outputOK = self.analysePerceptronTestElement(input, expectedOutput)
                if not outputOK:
                    errorElementsNumber += 1
                attributs = self.getNextRow()
            self.closeServerCursor()
        except Exception as exception :
            self.cursorConnection.rollback()
            raisedException = exception
        finally:
            if raisedException :
                raise raisedException
        # return
        return errorElementsNumber
    def isPartiallyTrained(self):
        # initialization
        raisedException = None
        partiallyTrained = False
        try:
            # run over for each training input / expected output
            selectStatement = "DECLARE " + self.serverCursor + " CURSOR FOR SELECT TE.ORIGINAL_INPUT,TE.EXPECTED_OUTPUT FROM " + trainingElementTable + " TE JOIN " + trainingSessionElementTable + " TSE ON TE.ID=TSE.TRAINING_SET_ELEMENT_ID WHERE (NOT TSE.IS_TEST) AND TSE.PERCEPTRON_ID=%s"
            selectParameters = (self.perceptron.id,)
            attributs = self.getFisrstRow(selectStatement, selectParameters)
            while attributs:
                input, expectedOutput = self.getTestElement(attributs, 0)
                partiallyTrained = self.analysePerceptronTestElement(input, expectedOutput)
                if partiallyTrained:
                    break
                attributs = self.getNextRow()
            self.closeServerCursor()
        except Exception as exception :
            self.cursorConnection.rollback()
            raisedException = exception
        finally:
            if raisedException :
                raise raisedException
        # return
        return partiallyTrained
    def computeTestScore(self):
        # initialization
        errorElementIds = list()
        raisedException = None
        try:
            # reset test elements on errors
            self.resetElementsOnErrors(True)
            # select all training elements
            selectStatement = "DECLARE "+self.serverCursor+" CURSOR FOR SELECT TE.ID,TE.ORIGINAL_INPUT,TE.EXPECTED_OUTPUT FROM " + trainingElementTable + " TE JOIN " + trainingSessionTable + " TS ON TE.TRAINING_SET_ID=TS.TRAINING_SET_ID JOIN " + trainingSessionElementTable + " TSE ON TE.ID=TSE.TRAINING_SET_ELEMENT_ID WHERE TS.PERCEPTRON_ID=%s AND TSE.IS_TEST"
            selectParameters = (self.perceptron.id,)
            attributs = self.getFisrstRow(selectStatement, selectParameters)
            # list training elements on error
            # INFO : if memory issue on last update (switch error on true), split this update on many batches
            testElementsNumber = 0
            while attributs:
                input, expectedOutput = self.getTestElement(attributs, 1)
                outputOK = self.analysePerceptronTestElement(input, expectedOutput)
                if not outputOK:
                    errorElementIds.append(attributs[0])
                testElementsNumber += 1
                attributs = self.getNextRow()
            self.closeServerCursor()
            # update test elements on errors
            self.updateElementsOnErrors(errorElementIds)
            # update test score
            errorElementsNumber = len(errorElementIds)
            testScore = errorElementsNumber / testElementsNumber
            updateTestScore(self.extraCursor,self.perceptron.id, testScore)
            # INFO : extra commit while be done in trainer business class
            # validate all transactions
            self.cursorConnection.commit()
        except Exception as exception :
            self.cursorConnection.rollback()
            raisedException = exception
        finally:
            if raisedException :
                raise raisedException
        pass
    # constructors
    def __init__(self, perceptronId=None,extraConnection=None,extraCursor=None):
        PostgreSqlUnitaryReadProcessRowTable.__init__(self,connectDatabase())
        self.extraConnection = extraConnection
        self.extraCursor = extraCursor
        if extraCursor:
            self.perceptron = selectById(self.extraCursor,perceptronId,True)
    def cleanup(self):
        PostgreSqlUnitaryReadProcessRowTable.cleanup(self)
        self.perceptron.layerTrainingDB.cleanup()
    pass
pass