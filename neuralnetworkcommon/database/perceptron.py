# coding=utf-8
# import
from neuralnetworkcommon.database.database import schema
from neuralnetworkcommon.entity.perceptron.perceptron import Perceptron
from neuralnetworkcommon.entity.perceptron.perceptronSummary import PerceptronSummary
from neuralnetworkcommon.database.layer import table as layerTable
# perceptron
table = schema + ".PERCEPTRON"
def insert(cursor,perceptron):
    statement = "INSERT INTO "+table+" (WORKSPACE_ID,COMMENTS) VALUES (%s,%s) RETURNING ID"
    parameters = (perceptron.workspaceId,perceptron.comments,)
    cursor.execute(statement, parameters)
    id = cursor.fetchone()[0]
    perceptron.id = id
    pass
def selectById(cursor,id,training=False):
    statement = "SELECT WORKSPACE_ID,COMMENTS FROM "+table+" WHERE ID=%s"
    parameters = (id,)
    cursor.execute(statement, parameters)
    attributs = cursor.fetchone()
    perceptron = None
    if attributs:
        perceptron = Perceptron(id,attributs[0],attributs[1],cursor) if training else Perceptron(id, attributs[0], attributs[1])
    return perceptron
def update(cursor,perceptron):
    statement = "UPDATE " + table + " SET WORKSPACE_ID=%s, COMMENTS=%s WHERE ID=%s"
    parameters = (perceptron.workspaceId, perceptron.comments, perceptron.id,)
    cursor.execute(statement, parameters)
    pass
def deleteById(cursor,id):
    statement = "DELETE FROM " + table + " WHERE ID=%s"
    parameters = (id,)
    cursor.execute(statement, parameters)
    pass
def selectAllIdsByWorkspaceId(cursor,workspaceId):
    statement = "SELECT ID FROM "+table+" WHERE WORKSPACE_ID = %s ORDER BY ID ASC"
    parameters = (workspaceId,)
    cursor.execute(statement, parameters)
    attributs = cursor.fetchall()
    ids = [_[0] for _ in attributs] if attributs else list()
    return ids
def deleteAllByWorkspaceId(cursor,workspaceId):
    statement = "DELETE FROM " + table + " WHERE WORKSPACE_ID = %s"
    parameters = (workspaceId,)
    cursor.execute(statement, parameters)
    pass
def selectSummaryById(cursor,id):
    statement = "SELECT P.WORKSPACE_ID,COUNT(L.PERCEPTRON_ID),P.COMMENTS FROM "+table+" P LEFT JOIN " + layerTable + " L ON L.PERCEPTRON_ID=P.ID WHERE P.ID = %s GROUP BY P.WORKSPACE_ID,P.COMMENTS"
    parameters = (id,)
    cursor.execute(statement, parameters)
    attributs = cursor.fetchone()
    perceptronSummary = PerceptronSummary(id,attributs[0],attributs[1],attributs[2]) if attributs else None
    return perceptronSummary
def patch(cursor,perceptron):
    statement = "UPDATE " + table + " SET WORKSPACE_ID=%s, COMMENTS=%s WHERE ID=%s"
    parameters = (perceptron.workspaceId,perceptron.comments, perceptron.id,)
    cursor.execute(statement, parameters)
    pass
pass
