# coding=utf-8
# import
from os import environ
from psycopg2 import connect
# database
'''
INFO : I do not use any other SQL library such as
 - python-sql to construct SQL statement
 - SQLAlchemy for object mapping
because it can complexify code without any benefits at this moment
'''
# contants
host = environ.get("NEURALNETWORK_DATABASE_HOST")
port = environ.get("NEURALNETWORK_DATABASE_PORT")
dbname = environ.get("NEURALNETWORK_DATABASE_NAME")
schema = environ.get("NEURALNETWORK_DATABASE_SCHEMA")
user = environ.get("NEURALNETWORK_DATABASE_USER")
password = environ.get("NEURALNETWORK_DATABASE_PASSWORD")
# connect
def connectDatabase():
    connection = connect(
        host=host,
        port=port,
        dbname=dbname,
        user=user,
        password=password
    )
    return connection
pass
