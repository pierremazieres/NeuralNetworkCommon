# coding=utf-8
# import
from neuralnetworkcommon.database.database import schema
from neuralnetworkcommon.entity.trainingElement import TrainingElement
# training set element
table=schema+".TRAINING_SET_ELEMENT"
def insert(cursor,trainingElement):
    statement = "INSERT INTO "+table+" (TRAINING_SET_ID,ORIGINAL_INPUT,EXPECTED_OUTPUT) VALUES (%s,%s,%s) RETURNING ID"
    parameters = (trainingElement.trainingSetId, trainingElement.input, trainingElement.expectedOutput,)
    cursor.execute(statement, parameters)
    id = cursor.fetchone()[0]
    trainingElement.id = id
    pass
def selectById(cursor,id):
    statement = "SELECT TRAINING_SET_ID,ORIGINAL_INPUT,EXPECTED_OUTPUT FROM "+table+" WHERE ID=%s"
    parameters = (id,)
    cursor.execute(statement, parameters)
    attributs = cursor.fetchone()
    trainingElement = None
    if attributs:
        input = [float(_) for _ in attributs[1]]
        expectedOutput = [float(_) for _ in attributs[2]]
        trainingElement = TrainingElement(id, attributs[0], input, expectedOutput)
    return trainingElement
def update(cursor,trainingElement):
    statement = "UPDATE "+table+" SET TRAINING_SET_ID=%s, ORIGINAL_INPUT=%s, EXPECTED_OUTPUT=%s WHERE ID=%s"
    parameters = (trainingElement.trainingSetId, trainingElement.input, trainingElement.expectedOutput, trainingElement.id,)
    cursor.execute(statement, parameters)
    pass
def deleteById(cursor,id):
    statement = "DELETE FROM "+table+" WHERE ID=%s"
    parameters = (id,)
    cursor.execute(statement, parameters)
    pass
def selectAllByTrainingSetIdAndFilter(cursor,trainingSetId,expectedOutput=None):
    statement = "SELECT ID FROM "+table+" WHERE TRAINING_SET_ID = %s"
    parameters = [trainingSetId]
    # where (if necessary)
    if expectedOutput:
        statement += " AND EXPECTED_OUTPUT = %s::numeric[]"
        parameters.append(expectedOutput)
    cursor.execute(statement, parameters)
    attributs = cursor.fetchall()
    ids = [_[0] for _ in attributs] if attributs else list()
    return ids
def deleteAllByTrainingSetId(cursor,trainingSetId):
    statement = "DELETE FROM "+table + " WHERE TRAINING_SET_ID = %s"
    parameters = (trainingSetId,)
    cursor.execute(statement,parameters)
    pass
pass
