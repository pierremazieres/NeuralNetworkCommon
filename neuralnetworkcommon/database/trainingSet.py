# coding=utf-8
# import
from neuralnetworkcommon.database.database import schema
from neuralnetworkcommon.entity.trainingSet.trainingSet import TrainingSet
from neuralnetworkcommon.entity.trainingSet.trainingSetSummary import TrainingSetSummary
from neuralnetworkcommon.database.trainingElement import table as trainingElementTable
# training set
table=schema+".TRAINING_SET"
def insert(cursor,trainingSet):
    # insert training set
    statement = "INSERT INTO "+table+" (WORKSPACE_ID,COMMENTS) VALUES (%s,%s) RETURNING ID"
    parameters = (trainingSet.workspaceId,trainingSet.comments,)
    cursor.execute(statement, parameters)
    id = cursor.fetchone()[0]
    trainingSet.id = id
    pass
def selectById(cursor,id):
    statement = "SELECT WORKSPACE_ID,COMMENTS FROM "+table+" WHERE ID=%s"
    parameters = (id,)
    cursor.execute(statement, parameters)
    attributs = cursor.fetchone()
    trainingSet = TrainingSet(id, attributs[0], attributs[1]) if attributs else None
    return trainingSet
def update(cursor,trainingSet):
    statement = "UPDATE "+table+" SET WORKSPACE_ID=%s, COMMENTS=%s WHERE ID=%s"
    parameters = (trainingSet.workspaceId, trainingSet.comments, trainingSet.id,)
    cursor.execute(statement, parameters)
    pass
def deleteById(cursor,id):
    statement = "DELETE FROM " + table + " WHERE ID=%s"
    parameters = (id,)
    cursor.execute(statement, parameters)
    pass
def selectAllIdsByWorkspaceId(cursor,workspaceId):
    statement = "SELECT ID FROM "+table+" WHERE WORKSPACE_ID = %s ORDER BY ID ASC"
    parameters = (workspaceId,)
    cursor.execute(statement,parameters)
    attributs = cursor.fetchall()
    ids = [_[0] for _ in attributs] if attributs else list()
    return ids
def deleteAllByWorkspaceId(cursor,workspaceId):
    statement = "DELETE FROM " + table + " WHERE WORKSPACE_ID = %s"
    parameters = (workspaceId,)
    cursor.execute(statement,parameters)
    pass
def selectSummaryById(cursor,id):
    # select training set summary
    statement = "SELECT TST.WORKSPACE_ID, COUNT(TSTE.ID), TST.COMMENTS FROM "+table+" TST LEFT JOIN "+trainingElementTable+" TSTE ON TSTE.TRAINING_SET_ID=TST.ID WHERE TST.ID=%s GROUP BY TST.WORKSPACE_ID, TST.COMMENTS"
    parameters = (id,)
    cursor.execute(statement, parameters)
    attributs = cursor.fetchone()
    trainingSetSummary = TrainingSetSummary(id,attributs[0], attributs[1], attributs[2]) if attributs else None
    return trainingSetSummary
def patch(cursor,trainingSet):
    # insert training set
    statement = "UPDATE "+table+" SET WORKSPACE_ID=%s, COMMENTS=%s WHERE ID=%s"
    parameters = (trainingSet.workspaceId,trainingSet.comments, trainingSet.id,)
    cursor.execute(statement, parameters)
    pass
pass
