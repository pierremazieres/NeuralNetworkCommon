# coding=utf-8
# import
from neuralnetworkcommon.database.database import schema
from neuralnetworkcommon.entity.workspace import Workspace
# workspace
table= schema + ".WORKSPACE"
def insert(cursor,workspace):
    statement = "INSERT INTO "+table+" (COMMENTS) VALUES (%s) RETURNING ID"
    parameters = (workspace.comments,)
    cursor.execute(statement, parameters)
    id = cursor.fetchone()[0]
    workspace.id = id
    pass
def selectById(cursor,id):
    statement = "SELECT COMMENTS FROM "+table+" WHERE ID=%s"
    parameters = (id,)
    cursor.execute(statement, parameters)
    attributs = cursor.fetchone()
    workspace = Workspace(id,attributs[0]) if attributs else None
    return workspace
def update(cursor,workspace):
    statement = "UPDATE "+table+" SET COMMENTS=%s WHERE ID=%s"
    parameters = (workspace.comments,workspace.id)
    cursor.execute(statement, parameters)
    pass
def deleteById(cursor,id):
    statement = "DELETE FROM " + table + " WHERE ID=%s"
    parameters = (id,)
    cursor.execute(statement, parameters)
    pass
def selectAllIds(cursor):
    statement = "SELECT ID FROM "+table+" ORDER BY ID ASC"
    cursor.execute(statement)
    attributs = cursor.fetchall()
    ids = [_[0] for _ in attributs] if attributs else list()
    return ids
def deleteAll(cursor):
    statement = "DELETE FROM " + table
    cursor.execute(statement)
    pass
pass
