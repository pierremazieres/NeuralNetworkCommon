# coding=utf-8
# import
from neuralnetworkcommon.entity.layer.layer import Layer
from neuralnetworkcommon.database.layer import table, update
from pythoncommontools.database.postgresql import PostgreSqlUnitaryReadProcessRowTable
from neuralnetworkcommon.database.database import connectDatabase
# layer (training)
# INFO : each server cursor must be reset after each end of loop in order to read updated values next time
class LayerTrainingDB(PostgreSqlUnitaryReadProcessRowTable):
    def passForward(self,vector,training=False):
        raisedException = None
        try:
            # select all layers (in order)
            selectStatement = "DECLARE " + self.serverCursor + " CURSOR FOR SELECT WEIGHTS,BIASES,DEPTH_INDEX FROM "+table+" WHERE PERCEPTRON_ID=%s ORDER BY DEPTH_INDEX ASC"
            selectParameters = (self.perceptronId,)
            attributs = self.getFisrstRow(selectStatement, selectParameters)
            # TODO : add a pipe to cascade passes (only if not training)
            while attributs:
                # pass forward
                weights = [[float(__) for __ in _] for _ in attributs[0]]
                biases = [float(_) for _ in attributs[1]]
                depthIndex = attributs[2]
                layer = Layer(self.perceptronId, depthIndex, weights, biases)
                # INFO : next input is actual output
                vector = layer.passForward(vector, training)
                # save training draft & next layer
                if training:
                    self.trainingDraft[depthIndex] = layer.trainingDraft
                attributs = self.getNextRow()
            self.closeServerCursor()
        except Exception as exception :
            self.cursorConnection.rollback()
            raisedException = exception
        finally:
            if raisedException :
                raise raisedException
        return vector
    def passBackward(self,expectedOutput):
        lastLayer = True
        raisedException = None
        try:
            # select all layers (reverse order)
            selectStatement = "DECLARE " + self.serverCursor + " CURSOR FOR SELECT WEIGHTS,BIASES,DEPTH_INDEX FROM "+table+" WHERE PERCEPTRON_ID=%s ORDER BY DEPTH_INDEX DESC"
            selectParameters = (self.perceptronId,)
            attributs = self.getFisrstRow(selectStatement, selectParameters)
            while attributs:
                # pass backward
                weights = [[float(__) for __ in _] for _ in attributs[0]]
                biases = [float(_) for _ in attributs[1]]
                depthIndex = attributs[2]
                layer = Layer(self.perceptronId, depthIndex, weights, biases)
                layer.trainingDraft = self.trainingDraft[depthIndex]
                if lastLayer:
                    differentialErrorWeightsBiasInput, previousLayerWeights = layer.passBackwardLastLayer(expectedOutput)
                    lastLayer = False
                else:
                    differentialErrorWeightsBiasInput, previousLayerWeights = layer.passBackwardHiddenLayer(differentialErrorWeightsBiasInput,previousLayerWeights)
                # save layer
                # WARNING : do not use self connection otherwise server cursor will be reset
                update(self.extraCursor,layer)
                # INFO : commit while be done in trainer DB class
                # next layer
                attributs = self.getNextRow()
            self.closeServerCursor()
        except Exception as exception :
            self.cursorConnection.rollback()
            raisedException = exception
        finally:
            if raisedException :
                raise raisedException
        pass
    def __init__(self, perceptronId=None,extraCurser=None):
        PostgreSqlUnitaryReadProcessRowTable.__init__(self,connectDatabase())
        self.extraCursor = extraCurser
        self.perceptronId = perceptronId
        self.trainingDraft = dict()
    def cleanup(self):
        PostgreSqlUnitaryReadProcessRowTable.cleanup(self)
    pass
pass