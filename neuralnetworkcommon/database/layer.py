# coding=utf-8
# import
from neuralnetworkcommon.database.database import schema
from random import random
from neuralnetworkcommon.entity.layer.layer import Layer
from neuralnetworkcommon.entity.layer.layerSummary import LayerSummary
# layer (WebService)
table = schema + ".LAYER"
def insert(cursor,layer, previousDimension, currentDimension):
    '''
    TODO : add extras parameters (uncertainties/dilatations/offsets)
    all parameters should be randomized between some given ranges
    TODO : parallelize random array generation
    '''
    weights = [[(random() - .5) * 2 for _ in range(previousDimension)] for _ in range(currentDimension)]
    biases = [0] * currentDimension
    statement = "INSERT INTO "+table+" (PERCEPTRON_ID,DEPTH_INDEX,WEIGHTS,BIASES) VALUES (%s,%s,%s,%s)"
    parameters = (layer.perceptronId, layer.depthIndex, weights, biases)
    cursor.execute(statement,parameters)
    pass
def selectByPerceptronIdAndDepthIndex(cursor,perceptronId, depthIndex):
    statement = "SELECT WEIGHTS,BIASES FROM "+table+" WHERE PERCEPTRON_ID=%s AND DEPTH_INDEX=%s"
    parameters = (perceptronId,depthIndex,)
    cursor.execute(statement, parameters)
    attributs = cursor.fetchone()
    layer = None
    if attributs:
        weights = [[float(__) for __ in _] for _ in attributs[0]]
        biases = [float(_) for _ in attributs[1]]
        layer = Layer(perceptronId, depthIndex,weights, biases)
    return layer
def update(cursor,layer):
    statement = "UPDATE " + table + " SET WEIGHTS=%s,BIASES=%s WHERE PERCEPTRON_ID=%s AND DEPTH_INDEX=%s"
    layer.weights = [[float(__) for __ in _] for _ in layer.weights]
    layer.biases = [float(_) for _ in layer.biases]
    parameters = (layer.weights, layer.biases, layer.perceptronId, layer.depthIndex,)
    cursor.execute(statement,parameters)
    pass
def deleteByPerceptronIdAndDepthIndex(cursor,perceptronId, depthIndex):
    statement = "DELETE FROM "+table+" WHERE PERCEPTRON_ID=%s AND DEPTH_INDEX=%s"
    parameters = (perceptronId,depthIndex,)
    cursor.execute(statement, parameters)
    pass
def selectAllIdsByPerceptronId(cursor,perceptronId):
    statement = "SELECT DEPTH_INDEX FROM " + table + " WHERE PERCEPTRON_ID=%s"
    parameters = (perceptronId,)
    cursor.execute(statement,parameters)
    attributs = cursor.fetchall()
    ids = [_[0] for _ in attributs] if attributs else list()
    return ids
def deleteAllByPerceptronId(cursor,perceptronId):
    statement = "DELETE FROM " + table + " WHERE PERCEPTRON_ID=%s"
    parameters = (perceptronId,)
    cursor.execute(statement, parameters)
    pass
def selectSummaryByPerceptronIdAndDepthIndex(cursor,perceptronId, depthIndex):
    statement = "SELECT ARRAY_LENGTH(WEIGHTS,1),ARRAY_LENGTH(WEIGHTS,2),ARRAY_LENGTH(BIASES,1) FROM "+table+" WHERE PERCEPTRON_ID=%s AND DEPTH_INDEX=%s"
    parameters = (perceptronId,depthIndex,)
    cursor.execute(statement, parameters)
    attributs = cursor.fetchone()
    layerSummary = LayerSummary(perceptronId, depthIndex,[attributs[0], attributs[1]], attributs[2]) if attributs else None
    return layerSummary
pass
