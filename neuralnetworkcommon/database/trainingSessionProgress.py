# coding=utf-8
# import
from neuralnetworkcommon.database.database import schema
from neuralnetworkcommon.entity.trainingSessionProgress import TrainingSessionProgress
from neuralnetworkcommon.database.perceptron import selectAllIdsByWorkspaceId, table as perceptronTable
# training progress
table=schema+".TRAINING_SESSION_PROGRESS"
def insert(cursor,trainingSessionProgress):
    # cast arrays elements to float
    statement = "INSERT INTO "+table+" (PERCEPTRON_ID,MEAN_DIFFERENTIAL_ERRORS,ERROR_ELEMENTS_NUMBERS,RESETS) VALUES (%s,%s,%s,%s);"
    parameters = (trainingSessionProgress.perceptronId, trainingSessionProgress.meanDifferentialErrors, trainingSessionProgress.errorElementsNumbers, trainingSessionProgress.resets,)
    cursor.execute(statement, parameters)
def initialize(cursor,perceptronId,meanDifferentialError, errorElementsNumber,reset):
    statement = "INSERT INTO "+table+" (PERCEPTRON_ID,MEAN_DIFFERENTIAL_ERRORS,ERROR_ELEMENTS_NUMBERS,RESETS) VALUES (%s,%s,%s,%s);"
    parameters = (perceptronId, [meanDifferentialError], [errorElementsNumber], [reset],)
    cursor.execute(statement, parameters)
def append(cursor,perceptronId,meanDifferentialError, errorElementsNumber, reset):
    statement = "UPDATE "+table+" SET MEAN_DIFFERENTIAL_ERRORS=MEAN_DIFFERENTIAL_ERRORS||%s,ERROR_ELEMENTS_NUMBERS=ERROR_ELEMENTS_NUMBERS||%s,RESETS=RESETS||%s WHERE PERCEPTRON_ID=%s;"
    parameters = (meanDifferentialError, errorElementsNumber, reset,perceptronId,)
    cursor.execute(statement, parameters)
def selectByPerceptronId(cursor,perceptronId):
    # select all training progress
    statement = "SELECT MEAN_DIFFERENTIAL_ERRORS, ERROR_ELEMENTS_NUMBERS, RESETS FROM "+table+" WHERE PERCEPTRON_ID=%s"
    parameters = (perceptronId,)
    trainingSessionProgress = None
    cursor.execute(statement, parameters)
    attributs = cursor.fetchone()
    # get training session information
    if attributs:
        meanDifferentialErrors = [float(_) for _ in attributs[0]] if attributs[0] else None
        trainingSessionProgress = TrainingSessionProgress(perceptronId,meanDifferentialErrors,attributs[1],attributs[2])
    return trainingSessionProgress
def update(cursor,trainingSessionProgress):
    statement = "UPDATE " + table + " SET MEAN_DIFFERENTIAL_ERRORS=%s,ERROR_ELEMENTS_NUMBERS=%s,RESETS=%s WHERE PERCEPTRON_ID=%s;"
    parameters = (trainingSessionProgress.meanDifferentialErrors, trainingSessionProgress.errorElementsNumbers, trainingSessionProgress.resets, trainingSessionProgress.perceptronId,)
    cursor.execute(statement, parameters)
    pass
def deleteByPerceptronId(cursor,perceptronId):
    statement = "DELETE FROM "+table+" WHERE PERCEPTRON_ID=%s"
    parameters = (perceptronId,)
    cursor.execute(statement, parameters)
    pass
def selectAllIdsByWorkspaceId(cursor,workspaceId):
    statement = "SELECT TSP.PERCEPTRON_ID FROM "+table+" TSP JOIN " + perceptronTable + " P ON TSP.PERCEPTRON_ID=P.ID WHERE P.WORKSPACE_ID = %s ORDER BY TSP.PERCEPTRON_ID ASC"
    parameters = (workspaceId,)
    cursor.execute(statement,parameters)
    attributs = cursor.fetchall()
    ids = [_[0] for _ in attributs] if attributs else list()
    return ids
def deleteAllByWorkspaceId(cursor,workspaceId):
    # delete all training progresss
    perceptronIds = selectAllIdsByWorkspaceId(cursor,workspaceId)
    # delete all related data
    if len(perceptronIds) > 0:
        statement = "DELETE FROM " + table + " WHERE PERCEPTRON_ID IN (" + ','.join(['%s']*len(perceptronIds)) + ')'
        cursor.execute(statement,perceptronIds)
    pass
def shrinkByPerceptronId(cursor,perceptronId, shrinkStep):
    # select actual training progress
    trainingSessionProgress = selectByPerceptronId(cursor,perceptronId)
    # remove data
    currentIndex = len(trainingSessionProgress.meanDifferentialErrors) - shrinkStep
    while currentIndex > 0:
        del trainingSessionProgress.meanDifferentialErrors[int(currentIndex)]
        currentIndex -= shrinkStep
    currentIndex = len(trainingSessionProgress.errorElementsNumbers) - shrinkStep
    while currentIndex > 0:
        del trainingSessionProgress.errorElementsNumbers[int(currentIndex)]
        currentIndex -= shrinkStep
    currentIndex = len(trainingSessionProgress.resets) - shrinkStep
    while currentIndex > 0:
        del trainingSessionProgress.resets[int(currentIndex)]
        currentIndex -= shrinkStep
    # update actual training progress
    update(cursor,trainingSessionProgress)
pass
