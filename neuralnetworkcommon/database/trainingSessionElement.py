# coding=utf-8
# import
from neuralnetworkcommon.database.database import schema
from neuralnetworkcommon.database.trainingElement import selectAllByTrainingSetIdAndFilter, table as trainingElementTable
from psycopg2.extras import execute_batch
from pythoncommontools.database.database import randomDbObjectName
from random import shuffle
# training element
table=schema+".TRAINING_SESSION_ELEMENT"
def insertByPerceptronAndTrainingSetIds(cursor,perceptronId, trainingSetId, testRatio):
    # separate training & test element
    trainingElementsIds = selectAllByTrainingSetIdAndFilter(cursor,trainingSetId)
    shuffle(trainingElementsIds)
    switchTrainingTestIndex = int(len(trainingElementsIds)*testRatio)
    parametersMatrix = list()
    for index, trainingElementId in enumerate(trainingElementsIds):
        test = index < switchTrainingTestIndex
        parametersLine = (perceptronId, trainingElementId, test,)
        parametersMatrix.append(parametersLine)
        pass
    # insert training element
    plan = randomDbObjectName()
    statement = "PREPARE " + plan + " (INTEGER,INTEGER,BOOLEAN) AS INSERT INTO "+table+" (PERCEPTRON_ID,TRAINING_SET_ELEMENT_ID,IS_TEST) VALUES ($1,$2,$3);"
    cursor.execute(statement)
    execute_batch(cursor, "EXECUTE "+plan+" (%s,%s,%s)", parametersMatrix)
    cursor.execute("DEALLOCATE "+plan)
    pass
def updateByPerceptronAndTrainingSetIds(cursor,perceptronId, trainingSetId, testRatio):
    # delete all training elements
    # INFO : we do not want any old training elements to remains if there is less training elements
    deleteByPerceptronId(cursor,perceptronId)
    # insert each training element
    insertByPerceptronAndTrainingSetIds(cursor,perceptronId, trainingSetId, testRatio)
    pass
def selectAllByPerceptronIdAndFilter(cursor,perceptronId,expectedOutput=None,testFilter=None,errorFilter=None):
    # select all training elements
    statement = "SELECT TSE.TRAINING_SET_ELEMENT_ID FROM "+table + " TSE"
    parameters = [perceptronId]
    # join (if necessary)
    if expectedOutput:
        statement += " JOIN "+ trainingElementTable + " TE ON TSE.TRAINING_SET_ELEMENT_ID=TE.ID"
    # where (if necessary)
    statement += " WHERE TSE.PERCEPTRON_ID = %s"
    if testFilter is not None:
        if testFilter:
            statement += " AND TSE.IS_TEST"
        else:
            statement += " AND (NOT TSE.IS_TEST)"
    if errorFilter is not None:
        if errorFilter:
            statement += " AND TSE.ON_ERROR"
        else:
            statement += " AND (NOT TSE.ON_ERROR)"
    if expectedOutput:
        statement += " AND TE.EXPECTED_OUTPUT = %s::numeric[]"
        parameters.append(expectedOutput)
    # execute request
    cursor.execute(statement, parameters)
    attributs = cursor.fetchall()
    trainingElementsIds = [_[0] for _ in attributs] if attributs else list()
    return trainingElementsIds
def deleteByPerceptronId(cursor,perceptronId):
    statement = "DELETE FROM "+table+" WHERE PERCEPTRON_ID=%s"
    parameters = (perceptronId,)
    cursor.execute(statement, parameters)
    pass
def deleteAllByPerceptronIds(cursor,perceptronIds):
    statement = "DELETE FROM "+table + " WHERE PERCEPTRON_ID IN (" + ','.join(['%s']*len(perceptronIds)) + ')'
    cursor.execute(statement,perceptronIds)
    pass
pass
