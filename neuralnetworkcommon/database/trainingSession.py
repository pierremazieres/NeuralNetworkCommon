# coding=utf-8
# import
from neuralnetworkcommon.database.database import schema
from neuralnetworkcommon.database.perceptron import selectAllIdsByWorkspaceId, table as perceptronTable
from neuralnetworkcommon.entity.trainingSession.trainingSession import TrainingSession
from neuralnetworkcommon.entity.trainingSession.trainingSessionSummary import TrainingSessionSummary, ElementsNumber
from neuralnetworkcommon.database.trainingSessionProgress import shrinkByPerceptronId, deleteByPerceptronId as trainingSessionProgressDeleteByPerceptronId, table as trainingSessionProgressTable
from neuralnetworkcommon.database.trainingSessionElement import insertByPerceptronAndTrainingSetIds, updateByPerceptronAndTrainingSetIds, deleteByPerceptronId as trainingSessionElementDeleteByPerceptronId, deleteAllByPerceptronIds, table as trainingSessionElementTable
# training session
table=schema+".TRAINING_SESSION"
def insert(cursor,trainingSession,testRatio):
    statement = "INSERT INTO "+table+" (PERCEPTRON_ID,TRAINING_SET_ID,SAVE_INTERVAL,MAXIMUM_TRY,MAXIMUM_ERROR_RATIO,COMMENTS) VALUES (%s,%s,%s,%s,%s,%s)"
    parameters = (trainingSession.perceptronId, trainingSession.trainingSetId, trainingSession.saveInterval, trainingSession.maximumTry, trainingSession.maximumErrorRatio,trainingSession.comments,)
    cursor.execute(statement, parameters)
    insertByPerceptronAndTrainingSetIds(cursor,trainingSession.perceptronId, trainingSession.trainingSetId, testRatio)
    pass
def selectByPerceptronId(cursor,perceptronId):
    statement = "SELECT TRAINING_SET_ID,SAVE_INTERVAL,MAXIMUM_TRY,MAXIMUM_ERROR_RATIO,PID,ERROR,TEST_SCORE,COMMENTS FROM "+table+" WHERE PERCEPTRON_ID=%s"
    parameters = (perceptronId,)
    cursor.execute(statement, parameters)
    attributs = cursor.fetchone()
    trainingSession = None
    if attributs:
        maximumErrorRatio = float(attributs[3])
        testScore = float(attributs[6]) if attributs[6] else None
        trainingSession = TrainingSession(perceptronId,attributs[0],attributs[1],attributs[2],maximumErrorRatio,attributs[4],attributs[5],testScore,attributs[7])
    return trainingSession
def updateTestScore(cursor,perceptronId,testScore):
    statement = "UPDATE "+table+" SET TEST_SCORE=%s WHERE PERCEPTRON_ID=%s"
    parameters = (testScore,perceptronId,)
    cursor.execute(statement, parameters)
    pass
def updatePid(cursor,perceptronId,pid):
    statement = "UPDATE "+table+" SET PID=%s WHERE PERCEPTRON_ID=%s"
    parameters = (pid,perceptronId,)
    cursor.execute(statement, parameters)
    pass
def updateError(cursor,perceptronId,errorMessage):
    statement = "UPDATE "+table+" SET ERROR=%s WHERE PERCEPTRON_ID=%s"
    parameters = (errorMessage,perceptronId,)
    cursor.execute(statement, parameters)
    pass
def update(cursor,trainingSession, testRatio):
    # remove progress
    trainingSessionProgressDeleteByPerceptronId(cursor,trainingSession.perceptronId)
    # update all training elements
    updateByPerceptronAndTrainingSetIds(cursor,trainingSession.perceptronId, trainingSession.trainingSetId, testRatio)
    # update details
    statement = "UPDATE "+table+" SET TRAINING_SET_ID=%s,SAVE_INTERVAL=%s,MAXIMUM_TRY=%s,MAXIMUM_ERROR_RATIO=%s,PID=%s,ERROR=%s,TEST_SCORE=%s,COMMENTS=%s WHERE PERCEPTRON_ID=%s"
    parameters = (trainingSession.trainingSetId,trainingSession.saveInterval,trainingSession.maximumTry,trainingSession.maximumErrorRatio,trainingSession.pid,trainingSession.errorMessage,trainingSession.testScore,trainingSession.comments,trainingSession.perceptronId)
    cursor.execute(statement, parameters)
    pass
def deleteByPerceptronId(cursor,perceptronId):
    # delete all training elements
    trainingSessionElementDeleteByPerceptronId(cursor, perceptronId)
    # delete all training sessions
    statement = "DELETE FROM " + table + " WHERE PERCEPTRON_ID=%s"
    parameters = (perceptronId,)
    cursor.execute(statement, parameters)
    pass
def selectAllIdsByWorkspaceId(cursor,workspaceId):
    statement = "SELECT TS.PERCEPTRON_ID FROM "+table+" TS JOIN " + perceptronTable + " P ON TS.PERCEPTRON_ID=P.ID WHERE P.WORKSPACE_ID = %s ORDER BY TS.PERCEPTRON_ID ASC"
    parameters = (workspaceId,)
    cursor.execute(statement,parameters)
    attributs = cursor.fetchall()
    ids = [_[0] for _ in attributs] if attributs else list()
    return ids
def deleteAllByWorkspaceId(cursor,workspaceId):
    # delete all related data
    perceptronIds = selectAllIdsByWorkspaceId(cursor,workspaceId)
    # delete all training session
    if len(perceptronIds) > 0:
        deleteAllByPerceptronIds(cursor,perceptronIds)
        # delete all training set
        statement = "DELETE FROM " + table + " WHERE PERCEPTRON_ID IN (" + ','.join(['%s']*len(perceptronIds)) + ')'
        cursor.execute(statement,perceptronIds)
    pass
def selectSummaryByPerceptronId(cursor,perceptronId):
    # select perceptron summary
    statement = "SELECT TS.TRAINING_SET_ID,TS.SAVE_INTERVAL,TS.MAXIMUM_TRY,TS.MAXIMUM_ERROR_RATIO,SUM(CASE WHEN (NOT TSE.IS_TEST) AND (NOT TSE.ON_ERROR) THEN 1 ELSE 0 END),SUM(CASE WHEN (NOT TSE.IS_TEST) AND TSE.ON_ERROR THEN 1 ELSE 0 END),SUM(CASE WHEN TSE.IS_TEST AND (NOT TSE.ON_ERROR) THEN 1 ELSE 0 END),SUM(CASE WHEN TSE.IS_TEST AND TSE.ON_ERROR THEN 1 ELSE 0 END),TS.PID,TS.ERROR,ARRAY_LENGTH(TSP.MEAN_DIFFERENTIAL_ERRORS,1),TSP.MEAN_DIFFERENTIAL_ERRORS[ARRAY_LENGTH(TSP.MEAN_DIFFERENTIAL_ERRORS,1)],TSP.ERROR_ELEMENTS_NUMBERS[ARRAY_LENGTH(TSP.ERROR_ELEMENTS_NUMBERS,1)],TRUE = ANY(TSP.RESETS),TS.TEST_SCORE,TS.COMMENTS FROM " + table + " TS LEFT JOIN " + trainingSessionElementTable + " TSE ON TSE.PERCEPTRON_ID = TS.PERCEPTRON_ID LEFT JOIN " + trainingSessionProgressTable + " TSP ON TSP.PERCEPTRON_ID = TS.PERCEPTRON_ID WHERE TS.PERCEPTRON_ID=%s GROUP BY TS.TRAINING_SET_ID,TS.SAVE_INTERVAL,TS.MAXIMUM_TRY,TS.MAXIMUM_ERROR_RATIO,TS.PID,TS.ERROR,TSP.MEAN_DIFFERENTIAL_ERRORS,TSP.ERROR_ELEMENTS_NUMBERS,TSP.RESETS,TS.TEST_SCORE,TS.COMMENTS"
    parameters = (perceptronId,)
    perceptronSummary = None
    cursor.execute(statement, parameters)
    attributs = cursor.fetchone()
    # create perceptron summary if need
    if attributs:
        # set elements number
        elementsNumber = ElementsNumber(attributs[4],attributs[5],attributs[6],attributs[7])
        # construct & return perceptron summary
        attributs=list(attributs)
        attributs[3]=float(attributs[3])
        attributs[11] = float(attributs[11]) if attributs[11] else None
        attributs[14] = float(attributs[14]) if attributs[14] else None
        perceptronSummary = TrainingSessionSummary(perceptronId,attributs[0],attributs[1],attributs[2],attributs[3],elementsNumber,attributs[8],attributs[9],attributs[10],attributs[11],attributs[12],attributs[13],attributs[14],attributs[15])
    return perceptronSummary
def patch(cursor,trainingSession):
    # update all training progress (if needed)
    actualrainingSession = selectByPerceptronId(cursor,trainingSession.perceptronId)
    if trainingSession.saveInterval > actualrainingSession.saveInterval:
        shrinkStep = trainingSession.saveInterval / (trainingSession.saveInterval - actualrainingSession.saveInterval)
        shrinkByPerceptronId(cursor,trainingSession.perceptronId, shrinkStep)
    # update details
    statement = "UPDATE "+table+" SET SAVE_INTERVAL=%s,MAXIMUM_TRY=%s,MAXIMUM_ERROR_RATIO=%s,COMMENTS=%s WHERE PERCEPTRON_ID=%s"
    parameters = (trainingSession.saveInterval,trainingSession.maximumTry,trainingSession.maximumErrorRatio,trainingSession.comments,trainingSession.perceptronId)
    cursor.execute(statement, parameters)
    pass
pass
