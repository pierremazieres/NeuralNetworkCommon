# coding=utf-8
# import
from enum import Enum, unique
from pythoncommontools.service.httpSymbol import HttpSymbol, arrayToQuotedString, enquoteValue
from pythoncommontools.service.service import constructUrl
# contants
@unique
class CommonParameters(Enum):
    PERCEPTRON_ID = "perceptronid"
    WORKSPACE_ID = "workspaceid"
    PREVIOUS_DIMENSION = "previousdimension"
    CURRENT_DIMENSION = "currentdimension"
@unique
class TrainerParameters(Enum):
    TRAININGSET_ID = "trainingsetid"
    TEST_RATIO = "testratio"
    EXPECTED_OUTPUT = "expectedoutput"
    TEST_FILTER = "testfilter"
    ERROR_FILTER = "errorfilter"
    ACTION = "action"
@unique
class TrainerAction(Enum):
    START = "start"
    STOP = "stop"
@unique
class ResourceValueType(Enum):
    WORKSPACE="<int:workspaceId>"
    PERCEPTRON="<int:perceptronId>"
    DEPTH_INDEX="<int:depthIndex>"
@unique
class TrainerResourceValueType(Enum):
    TRAININGSET="<int:trainingSetId>"
    TRAININGELEMENT="<int:trainingElementId>"
@unique
class CommonPathType(Enum):
    SUMMARY = "summary"
    EXECUTE = "execute"
@unique
class ResourcePathType(Enum):
    GLOBAL_WORKSPACE="workspace"
    SPECIFIC_WORKSPACE=HttpSymbol.PATH_SEPARATOR.value.join((GLOBAL_WORKSPACE,ResourceValueType.WORKSPACE.value,))
    GLOBAL_PERCEPTRON="perceptron"
    SPECIFIC_PERCEPTRON=HttpSymbol.PATH_SEPARATOR.value.join((GLOBAL_PERCEPTRON,ResourceValueType.PERCEPTRON.value,))
    PERCEPTRON_SUMMARY=HttpSymbol.PATH_SEPARATOR.value.join((SPECIFIC_PERCEPTRON,CommonPathType.SUMMARY.value,))
    PERCEPTRON_EXECUTION=HttpSymbol.PATH_SEPARATOR.value.join((SPECIFIC_PERCEPTRON,CommonPathType.EXECUTE.value,))
    GLOBAL_LAYER="layer"
    SPECIFIC_LAYER=HttpSymbol.PATH_SEPARATOR.value.join((GLOBAL_LAYER,ResourceValueType.PERCEPTRON.value,ResourceValueType.DEPTH_INDEX.value,))
    LAYER_SUMMARY=HttpSymbol.PATH_SEPARATOR.value.join((SPECIFIC_LAYER,CommonPathType.SUMMARY.value,))
    pass
@unique
class TrainerResourcePathType(Enum):
    GLOBAL_TRAININGSET="trainingset"
    SPECIFIC_TRAININGSET=HttpSymbol.PATH_SEPARATOR.value.join((GLOBAL_TRAININGSET,TrainerResourceValueType.TRAININGSET.value,))
    TRAININGSET_SUMMARY=HttpSymbol.PATH_SEPARATOR.value.join((SPECIFIC_TRAININGSET,CommonPathType.SUMMARY.value,))
    GLOBAL_TRAININGELEMENT="trainingelement"
    SPECIFIC_TRAININGELEMENT=HttpSymbol.PATH_SEPARATOR.value.join((GLOBAL_TRAININGELEMENT,TrainerResourceValueType.TRAININGELEMENT.value,))
    GLOBAL_TRAININGSESSION="trainingsession"
    SPECIFIC_TRAININGSESSION=HttpSymbol.PATH_SEPARATOR.value.join((GLOBAL_TRAININGSESSION,ResourceValueType.PERCEPTRON.value,))
    TRAININGSESSION_SUMMARY=HttpSymbol.PATH_SEPARATOR.value.join((SPECIFIC_TRAININGSESSION,CommonPathType.SUMMARY.value,))
    TRAINER_EXECUTION=HttpSymbol.PATH_SEPARATOR.value.join((SPECIFIC_TRAININGSESSION,CommonPathType.EXECUTE.value,))
    GLOBAL_TRAININGSESSIONPROGRESS=GLOBAL_TRAININGSESSION+"progress"
    SPECIFIC_TRAININGSESSIONPROGRESS=HttpSymbol.PATH_SEPARATOR.value.join((GLOBAL_TRAININGSESSIONPROGRESS,ResourceValueType.PERCEPTRON.value,))
# resource URL
def globalWorkspaceUrl(endpoint):
    url = constructUrl(endpoint,ResourcePathType.GLOBAL_WORKSPACE.value)
    return url
def specificWorkspaceUrl(endpoint,workspaceId):
    url = constructUrl(endpoint, ResourcePathType.SPECIFIC_WORKSPACE.value, {ResourceValueType.WORKSPACE.value:workspaceId})
    return url
def globalPerceptronUrl(endpoint,workspaceId=None):
    parameters = {CommonParameters.WORKSPACE_ID.value:workspaceId} if workspaceId else None
    url = constructUrl(endpoint,ResourcePathType.GLOBAL_PERCEPTRON.value,parameters=parameters)
    return url
def specificPerceptronUrl(endpoint,perceptronId):
    url = constructUrl(endpoint, ResourcePathType.SPECIFIC_PERCEPTRON.value, {ResourceValueType.PERCEPTRON.value:perceptronId})
    return url
def perceptronSummaryUrl(endpoint,perceptronId):
    url = constructUrl(endpoint, ResourcePathType.PERCEPTRON_SUMMARY.value, {ResourceValueType.PERCEPTRON.value:perceptronId})
    return url
def perceptronExecutionUrl(endpoint,perceptronId):
    url = constructUrl(endpoint, ResourcePathType.PERCEPTRON_EXECUTION.value, {ResourceValueType.PERCEPTRON.value:perceptronId})
    return url
def globalLayerUrl(endpoint,perceptronId=None,previousDimension=None,currentDimension=None):
    parameters = None
    if perceptronId or previousDimension or currentDimension:
        parameters = dict()
        if perceptronId:
            parameters[CommonParameters.PERCEPTRON_ID.value] = perceptronId
        if previousDimension:
            parameters[CommonParameters.PREVIOUS_DIMENSION.value] = previousDimension
        if currentDimension:
            parameters[CommonParameters.CURRENT_DIMENSION.value] = currentDimension
    url = constructUrl(endpoint,ResourcePathType.GLOBAL_LAYER.value,parameters=parameters)
    return url
def specificLayerUrl(endpoint,perceptronId,depthIndex):
    url = constructUrl(endpoint, ResourcePathType.SPECIFIC_LAYER.value, {ResourceValueType.PERCEPTRON.value:perceptronId,ResourceValueType.DEPTH_INDEX.value:depthIndex})
    return url
def layerSummaryUrl(endpoint,perceptronId,depthIndex):
    url = constructUrl(endpoint, ResourcePathType.LAYER_SUMMARY.value, {ResourceValueType.PERCEPTRON.value:perceptronId,ResourceValueType.DEPTH_INDEX.value:depthIndex})
    return url
def globalTrainingSetUrl(endpoint,workspaceId=None):
    parameters = {CommonParameters.WORKSPACE_ID.value:workspaceId} if workspaceId else None
    url = constructUrl(endpoint,TrainerResourcePathType.GLOBAL_TRAININGSET.value,parameters=parameters)
    return url
def specificTrainingSetUrl(endpoint,trainingSetId):
    url = constructUrl(endpoint, TrainerResourcePathType.SPECIFIC_TRAININGSET.value, {TrainerResourceValueType.TRAININGSET.value:trainingSetId})
    return url
def trainingSetSummaryUrl(endpoint,trainingSetId):
    url = constructUrl(endpoint, TrainerResourcePathType.TRAININGSET_SUMMARY.value, {TrainerResourceValueType.TRAININGSET.value:trainingSetId})
    return url
def globalTrainingElementUrl(endpoint,trainingSetId=None,perceptronId=None,expectedOutput=None,testFilter=None,errorFilter=None):
    parameters = None
    if trainingSetId or perceptronId or expectedOutput or testFilter or errorFilter:
        parameters = dict()
        if trainingSetId:
            parameters[TrainerParameters.TRAININGSET_ID.value] = trainingSetId
        if perceptronId:
            parameters[CommonParameters.PERCEPTRON_ID.value] = perceptronId
        if expectedOutput:
            parameters[TrainerParameters.EXPECTED_OUTPUT.value] = enquoteValue(expectedOutput) if type(expectedOutput) == str else  arrayToQuotedString(expectedOutput)
        if testFilter:
            parameters[TrainerParameters.TEST_FILTER.value] = testFilter
        if errorFilter:
            parameters[TrainerParameters.ERROR_FILTER.value] = errorFilter
    url = constructUrl(endpoint,TrainerResourcePathType.GLOBAL_TRAININGELEMENT.value,parameters=parameters)
    return url
def specificTrainingElementUrl(endpoint,trainingElementId):
    url = constructUrl(endpoint, TrainerResourcePathType.SPECIFIC_TRAININGELEMENT.value, {TrainerResourceValueType.TRAININGELEMENT.value:trainingElementId})
    return url
def globalTrainingSessionUrl(endpoint,workspaceId=None,testRatio=None):
    parameters = None
    if workspaceId or testRatio:
        parameters = dict()
        if workspaceId:
            parameters[CommonParameters.WORKSPACE_ID.value] = workspaceId
        if testRatio:
            parameters[TrainerParameters.TEST_RATIO.value] = testRatio
    url = constructUrl(endpoint,TrainerResourcePathType.GLOBAL_TRAININGSESSION.value,parameters=parameters)
    return url
def specificTrainingSessionUrl(endpoint,perceptronId):
    url = constructUrl(endpoint, TrainerResourcePathType.SPECIFIC_TRAININGSESSION.value, {ResourceValueType.PERCEPTRON.value:perceptronId})
    return url
def trainingSessionSummaryUrl(endpoint,perceptronId):
    url = constructUrl(endpoint, TrainerResourcePathType.TRAININGSESSION_SUMMARY.value, {ResourceValueType.PERCEPTRON.value:perceptronId})
    return url
def globalTrainingSessionProgressUrl(endpoint,workspaceId=None):
    parameters = {CommonParameters.WORKSPACE_ID.value:workspaceId} if workspaceId else None
    url = constructUrl(endpoint,TrainerResourcePathType.GLOBAL_TRAININGSESSIONPROGRESS.value,parameters=parameters)
    return url
def specificTrainingSessionProgressUrl(endpoint,perceptronId):
    url = constructUrl(endpoint, TrainerResourcePathType.SPECIFIC_TRAININGSESSIONPROGRESS.value, {ResourceValueType.PERCEPTRON.value:perceptronId})
    return url
def executeTrainerUrl(endpoint,perceptronId,action=None):
    parameters = {TrainerParameters.ACTION.value:action} if action else None
    url = constructUrl(endpoint, TrainerResourcePathType.TRAINER_EXECUTION.value, {ResourceValueType.PERCEPTRON.value:perceptronId},parameters=parameters)
    return url
pass