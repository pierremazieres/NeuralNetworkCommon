# coding=utf-8
# import
from flask_restful import Resource
from psycopg2.pool import ThreadedConnectionPool
from neuralnetworkcommon.database.database import user, password, host, port, dbname
from os import environ
# contants
minimumConnection = environ.get("NEURALNETWORK_DATABASE_MINIMUM_CONNECTION")
maximumConnection = environ.get("NEURALNETWORK_DATABASE_MAXIMUM_CONNECTION")
# kill service 'gracefully'
''' INFO : 
 - do not mix with customStop to avoid stack overflow
 - default parameters signalnum,handler are unsed but keep for compatibility
'''
def customKill(_,__):
    # INFO : connection poll is automatically closed by Postgres framework
    try:
        ConnectionPooledResource.connectionPool.closeall()
    finally:
        raise SystemExit
    pass
# connection pooled resource
class ConnectionPooledResource(Resource):
    connectionPool = ThreadedConnectionPool(minimumConnection, maximumConnection, user=user, password=password, host=host, port=port, database=dbname)
    pass
pass