# coding=utf-8
# import
from numpy import array
from neuralnetworkcommon.entity.perceptron.perceptronCommon import PerceptronCommon
from neuralnetworkcommon.database.layerTraining import LayerTrainingDB
# perceptron
class Perceptron(PerceptronCommon):
    # constructors
    def __init__(self,id=None,workspaceId=None,comments='',cursor=None):
        PerceptronCommon.__init__(self,id,workspaceId,comments)
        # layer training DB
        if cursor:
            self.layerTrainingDB = LayerTrainingDB(id,cursor)
    def passForwardBackward(self,input,expectedOutput):
        # pass forward
        actualOutput = self.layerTrainingDB.passForward(array(input),True)
        # compute total error
        outputError = ( ( array(expectedOutput) - array(actualOutput) ) ** 2 ) / 2
        totalError = sum(outputError)
        # pass backward
        self.layerTrainingDB.passBackward(expectedOutput)
        # return
        return totalError
    pass
pass
