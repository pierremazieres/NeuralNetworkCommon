# coding=utf-8
# import
from neuralnetworkcommon.entity.perceptron.perceptronCommon import PerceptronCommon
# perceptron summary
class PerceptronSummary(PerceptronCommon):
    # constructors
    def __init__(self,id=None,workspaceId=None,layersNumber=None,comments=''):
        PerceptronCommon.__init__(self,id,workspaceId,comments)
        self.layersNumber=layersNumber
    pass
pass
