# coding=utf-8
# import
from neuralnetworkcommon.entity.trainingSession.trainingSessionCommon import TrainingSessionCommon
# training session
class TrainingSession(TrainingSessionCommon):
    # constructors
    def __init__(self,perceptronId=None,trainingSetId=None,saveInterval=0,maximumTry=0,maximumErrorRatio=0,pid=None,errorMessage=None,testScore=None,comments=''):
        TrainingSessionCommon.__init__(self,perceptronId,trainingSetId,saveInterval,maximumTry,maximumErrorRatio,pid,errorMessage,testScore,comments)
    pass
pass