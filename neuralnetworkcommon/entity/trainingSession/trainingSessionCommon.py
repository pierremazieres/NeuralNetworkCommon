# coding=utf-8
# import
from pythoncommontools.objectUtil.POPO import POPO
from abc import ABC
# training session common
class TrainingSessionCommon(ABC,POPO):
    def __init__(self,perceptronId,trainingSetId,saveInterval,maximumTry,maximumErrorRatio,pid,errorMessage,testScore,comments):
        self.perceptronId = perceptronId
        self.trainingSetId = trainingSetId
        self.saveInterval = saveInterval
        self.maximumTry = maximumTry
        self.maximumErrorRatio = maximumErrorRatio
        self.pid = pid
        self.errorMessage = errorMessage
        self.testScore = testScore
        self.comments = comments
    pass
pass