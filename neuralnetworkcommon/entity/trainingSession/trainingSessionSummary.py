# coding=utf-8
# import
from pythoncommontools.objectUtil.POPO import POPO
from neuralnetworkcommon.entity.trainingSession.trainingSessionCommon import TrainingSessionCommon
# training session summary
class ElementsNumber(POPO):
    def __init__(self,fineTraining=None,errorTraining=None, fineTest=None, errorTest=None):
        self.fineTraining = fineTraining
        self.errorTraining = errorTraining
        self.fineTest = fineTest
        self.errorTest = errorTest
class TrainingSessionSummary(TrainingSessionCommon):
    def __init__(self,perceptronId=None,trainingSetId=None,saveInterval=None,maximumTry=None,maximumErrorRatio=None,elementsNumber=None,pid=None,errorMessage=None,progressRecordsNumber=None,meanDifferentialErrors=None,errorElementsNumbers=None,resets=None,testScore=None,comments=''):
        TrainingSessionCommon.__init__(self,perceptronId,trainingSetId,saveInterval,maximumTry,maximumErrorRatio,pid,errorMessage,testScore,comments)
        self.elementsNumber = elementsNumber
        self.progressRecordsNumber = progressRecordsNumber
        self.meanDifferentialErrors = meanDifferentialErrors
        self.errorElementsNumbers = errorElementsNumbers
        self.resets = resets
    pass
pass