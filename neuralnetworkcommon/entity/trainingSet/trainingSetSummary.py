# coding=utf-8
# import
from neuralnetworkcommon.entity.trainingSet.trainingSetCommon import TrainingSetCommon
# training set summary
class TrainingSetSummary(TrainingSetCommon):
    # constructors
    def __init__(self,id=None,workspaceId=None,trainingElementsNumber=None,comments=''):
        TrainingSetCommon.__init__(self,id,workspaceId,comments)
        self.trainingElementsNumber=trainingElementsNumber
    pass
pass
