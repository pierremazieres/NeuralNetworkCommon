# coding=utf-8
# import
from pythoncommontools.objectUtil.POPO import POPO
from abc import ABC
# training set common
class TrainingSetCommon(ABC,POPO):
    # constructors
    def __init__(self,id,workspaceId,comments):
        self.id = id
        self.workspaceId = workspaceId
        self.comments=comments
    pass
pass
