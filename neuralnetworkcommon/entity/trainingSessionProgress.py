# coding=utf-8
# import
from pythoncommontools.objectUtil.POPO import POPO
# training session progress
class TrainingSessionProgress(POPO):
    def __init__(self,perceptronId=None,meanDifferentialErrors=None, errorElementsNumbers=None, resets=None):
        self.perceptronId = perceptronId
        self.meanDifferentialErrors = meanDifferentialErrors
        self.errorElementsNumbers = errorElementsNumbers
        self.resets = resets
    pass
pass