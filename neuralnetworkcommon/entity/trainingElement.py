# coding=utf-8
# import
from pythoncommontools.objectUtil.POPO import POPO
# training element
class TrainingElement(POPO):
    # constructors
    def __init__(self,id=None,trainingSetId=None,input=None,expectedOutput=None):
        self.id=id
        self.trainingSetId=trainingSetId
        self.input=input
        self.expectedOutput=expectedOutput
    pass
