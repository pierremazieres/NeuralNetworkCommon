# coding=utf-8
# import
from numpy import array
from neuralnetworkcommon.utilities.sigmoid import value, derivative
from neuralnetworkcommon.entity.layer.layerCommon import LayerCommon
# training draft
class TrainingDraft():
    def __init__(self,input,output):
        self.input = input
        self.output = output
# layer
class Layer(LayerCommon):
    # constructors
    def __init__(self,perceptronId=None, depthIndex=0,weights=[[]],biases=[]):
        LayerCommon.__init__(self,perceptronId, depthIndex)
        self.weights=weights
        self.biases=biases
    # computation
    def passForward(self,input,training=False):
        # compute ouput
        # TODO : compute with spark 'weightsBiasInput'
        weightsBiasInput = array(self.weights).dot(array(input)) + array(self.biases)
        output = value(weightsBiasInput)
        if training:
            self.trainingDraft = TrainingDraft(input, output)
        return output
    def differentialErrorOutput(self,expectedOutput):
        # TODO : compute with spark 'differentialError'
        differentialError = array(self.trainingDraft.output) - array(expectedOutput)
        return differentialError
    def computeNewWeights(self,differentialErrorLayer):
        differentialOutputWeightsBiasInput = derivative(array([self.trainingDraft.output]))
        # INFO : new differential error on layer will be used on next computation
        newDifferentialErrorWeightsBiases = (array(differentialErrorLayer) * differentialOutputWeightsBiasInput).T
        differentialErrorWeights = newDifferentialErrorWeightsBiases * array(self.trainingDraft.input)
        # TODO : optionaly correct oter metaparameters (offset, dilatation, ...)
        # INFO : old weights will be used on next computation
        oldWeights = self.weights
        # TODO : parametrize learning rate (here 0.5)
        self.weights = oldWeights - 0.5 * differentialErrorWeights
        return newDifferentialErrorWeightsBiases, oldWeights
    def computeNewBiases(self,differentialErrorWeightsBiases):
        # TODO : parametrize learning rate (here 0.5)
        newBiases = array(self.biases) - 0.5 * array(differentialErrorWeightsBiases).T
        # INFO : sometimes, new biases are like a 1*N matrix (and not just a N vector)
        if len(newBiases.shape)==2 and newBiases.shape[0]==1:
            newBiases=newBiases[0]
        self.biases = newBiases
    def passBackwardLastLayer(self,expectedOutput):
        # TODO : compute with spark each parameter
        # TODO : parametrize learning rate (here 0.5)
        # TODO : add inertia
        # get differential error on layer regarding output or hidden one
        differentialError = self.differentialErrorOutput(expectedOutput)
        # compute new weights & biases
        newDifferentialErrorWeightsBiases, oldWeights = self.computeNewWeights(differentialError)
        self.computeNewBiases(newDifferentialErrorWeightsBiases)
        # return
        return newDifferentialErrorWeightsBiases, oldWeights
    def passBackwardHiddenLayer(self,differentialErrorWeightsBiasInput,previousLayerWeights):
        # TODO : compute with spark each parameter
        # TODO : parametrize learning rate (here 0.5)
        # TODO : add inertia
        # TODO : compute with spark 'differentialError'
        # get differential error on layer regarding output or hidden one
        differentialErrors = array(differentialErrorWeightsBiasInput) * array(previousLayerWeights)
        differentialError = sum(differentialErrors, 0)
        # compute new weights & biases
        newDifferentialErrorWeightsBiases, oldWeights = self.computeNewWeights(differentialError)
        self.computeNewBiases(newDifferentialErrorWeightsBiases)
        # return
        return newDifferentialErrorWeightsBiases, oldWeights
    pass
pass