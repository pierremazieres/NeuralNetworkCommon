# coding=utf-8
# import
from neuralnetworkcommon.entity.layer.layerCommon import LayerCommon
# layer summary
class LayerSummary(LayerCommon):
    # constructors
    def __init__(self,perceptronId=None,depthIndex=None,weightsDimensions=None,biasesDimension=None):
        LayerCommon.__init__(self,perceptronId, depthIndex)
        self.weightsDimensions=weightsDimensions
        self.biasesDimension=biasesDimension
    pass
pass