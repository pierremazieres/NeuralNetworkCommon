# coding=utf-8
# import
from pythoncommontools.objectUtil.POPO import POPO
from abc import ABC
# layer common
class LayerCommon(ABC,POPO):
    # constructors
    def __init__(self,perceptronId, depthIndex):
        self.perceptronId=perceptronId
        self.depthIndex=depthIndex
    pass
pass