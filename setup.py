#!/usr/bin/env python3
# coding=utf-8
# import
from setuptools import setup, find_packages
# define setup parameters
# for version norm, see : https://www.python.org/dev/peps/pep-0440/#post-releases
setup(
    name="NeuralNetworkCommon",
    version="0.0.2",
    description="Neural network common tools",
    packages=find_packages(),
    install_requires=["PythonCommonTools","numpy","psycopg2","psycopg2-binary","flask_restful","flask"],
    classifiers=["Programming Language :: Python :: 3"]
)
